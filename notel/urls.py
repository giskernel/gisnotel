"""notel URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/2.2/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.contrib import admin
from django.urls import path
from django.conf.urls import include
from django.contrib.auth import views as auth_views
from app import views as app_views
from django.conf import settings
from django.conf.urls.static import static

urlpatterns = [


    path('captchaaaaaa/', include('captcha.urls')),
    # path('', auth_views.LoginView.as_view(
    #     template_name="nongis/index.html"), name='login'),

    path('', app_views.user_login, name='login'),
    path('logout/', auth_views.LogoutView.as_view(template_name="nongis/logout.html"), name='logout'),
    path('password-reset/', auth_views.PasswordResetView.as_view(
        template_name='nongis/password_reset.html'), name='password_reset'),
    path('password-reset/done/', auth_views.PasswordChangeDoneView.as_view(
        template_name='nongis/password_reset_done.html'), name='password_reset_done'),
    path('password-reset-confirm/<uidb64>/<token>/', auth_views.PasswordResetConfirmView.as_view(
        template_name='nongis/password_reset_confirm.html'), name='password_reset_confirm'),
    path('password-reset-complete/', auth_views.PasswordResetCompleteView.as_view(
        template_name='nongis/password_reset_complete.html'), name='password_reset_complete'),
    path('admin/', admin.site.urls, name='defadmin'),
    path('map/', app_views.map, name='map'),
    path('register/', app_views.register, name='register'),
    path('profile/', app_views.profile, name='profile'),
    path('layerlistinformation/', app_views.layerlistinformation,
         name='layerlistinformation'),

    path('layerlistinformationmysql/', app_views.layerlistinformationmysql,
         name='layerlistinformationmysql'),
    path('getstore/', app_views.getstore, name='getstore'),
    # path('profile/edit/', app_views.update_profile, name='profile_edit'),
    path('profile/edit/<user_id>/', app_views.update_profile, name='profile_edit'),
    path('allcountries/', app_views.allcountries, name='allcountries'),
    path('alloperators/', app_views.alloperators, name='alloperators'),
    path('updateinfo/', app_views.updateinfo, name='updateinfo'),
    path('useractiveness/', app_views.useractiveness, name='useractiveness'),
    path('layerlists/', app_views.layerlists, name='layerlists'),
    path('mapwithnewcontent/', app_views.mapwithnewcontent,
         name='mapwithnewcontent'),
    path('forbulkuploaduserbasic/', app_views.forbulkuploaduserbasic,
         name='forbulkuploaduserbasic'),
    path('idofbulkcreateduser/', app_views.idofbulkcreateduser,
         name='idofbulkcreateduser'),
    path('uploaduserprofileinfoforbulkupload/', app_views.uploaduserprofileinfoforbulkupload,
         name='uploaduserprofileinfoforbulkupload'),
    path('updateid/', app_views.updateid, name='updateid'),
    path('specificprofilemodal/', app_views.specificprofilemodal,
         name='specificprofilemodal'),
    path('forsavingqueryindb/', app_views.forsavingqueryindb,
         name='forsavingqueryindb'),
    path('forsavinglayerindb/', app_views.forsavinglayerindb,
         name='forsavinglayerindb'),
    path('getUserActiveness/', app_views._getUserActiveness,
         name='getUserActiveness'),
    path('_getAllUserbase/', app_views._getAllUserbase,
         name='_getAllUserbase'),
    path('forqueryforallattrqueries/', app_views.forqueryforallattrqueries,
         name='forqueryforallattrqueries'),
    path('forqueryforallaliasnames/', app_views.forqueryforallaliasnames,
         name='forqueryforallaliasnames'),

    path('forqueryforallattrqueriessp/', app_views.forqueryforallattrqueriessp,
         name='forqueryforallattrqueriessp'),

    path('queryforvalues/', app_views.queryforvalues,
         name='queryforvalues'),

    path('rightclickinfo/', app_views.rightclickinfo,
         name='rightclickinfo'),
    path('queryforcolumnnames/', app_views.queryforcolumnnames,
         name='queryforcolumnnames'),
    path('queryforcolumnnames/', app_views.queryforcolumnnames,
         name='queryforcolumnnames'),
    path('allQuery/', app_views.allQuery,
         name='allQuery'),
    path('SpatilQuery/', app_views.SpatilQuery,
         name='SpatilQuery'),
    path('getallcells/', app_views.getallcells,
         name='getallcells'),
    path('getallcellsmysql/', app_views.getallcells,
         name='getallcellsmysql'),
    path('getallci/', app_views.getallci,
         name='getallci'),
    path('_getAllUserProfiles/', app_views._getAllUserProfiles,
         name='_getAllUserProfiles'),


    path('changebulkuser/', app_views.changebulkuser,
         name='changebulkuser'),
    path('layerconfigview/', app_views.layerconfigview, name='layerconfigview'),
    path('searchLayer/', app_views.searchLayer, name='searchLayer'),
    path('getTimeBinFields/', app_views.getTimeBinFields, name='getTimeBinFields'),
    path('getTimeBinData/', app_views.getTimeBinData, name='getTimeBinData'),
    path('getIdentifyFields/', app_views.getIdentifyFields,
         name='getIdentifyFields'),
    path('getSearchFields/', app_views.getSearchFields, name='getSearchFields'),
    path('getGeoBins/', app_views.getGeoBins, name='getGeoBins'),
    path('getLayerOperators/', app_views.getLayerOperators, name='getLayerOperators'),
    path('getBinningLayers/', app_views.getBinningLayers, name='getBinningLayers')
]

if settings.DEBUG:
    urlpatterns += static(settings.MEDIA_URL,
                          document_root=settings.MEDIA_ROOT)
