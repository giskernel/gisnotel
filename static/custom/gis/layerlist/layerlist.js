var noOfZoomLevels = 4
var getAllUserbase
var hostUrl = 'https://rfb.bounceme.net:2000/geoserver'; // for geoserver
var host_ip = 'https://rfb.bounceme.net:2000/'; // for wfs wms calls
var allocatedlayenames = []
var alllayerinfofromgetcapabilites, eleforzindex, Layernameforattribute
var alloperatorslength = 1271
var allOpList = [];
var layerdata, thecheckbox
var _mconf_attrQuery, _querystring_attrQuery = '', _recentquerystring_attrQuery = '', _zoomextent_attrQuery, _radiobtn_attrQuery, _layername_attrQuery, _extgeom_attrQuery, _fext_attrQuery, _flagfirstload_attrQuery = true, _fullextent_attrQuery, _flagfullextent_attrQuery = false, _flaggeomdraw_attrQuery = false;
var flagofneedtoCreateTable = true, infocomingfromlist = true
var dropdownlayers = ['cell_modified_freqn']
// var onLayers = [];      // layers which are checked

var allcolorRangesForBinning
$.getJSON('layerconfigview', function (data) {
        allcolorRangesForBinning = data.colorForBins
})
$("#LLModal").on('show.bs.modal', function () {
        //  $("#layerlistdiv").html("")
        //     $("#layersublistdiv").html("")
        if ($('#timefrom')[0].value == "") {
                $('#timefrom')[0].value = new Date(Date.now() - 7 * 24 * 60 * 60 * 1000).toISOString().substr(0, 10);

        }
        if ($('#timetill')[0].value == "") {
                $('#timetill')[0].value = new Date().toISOString().substr(0, 10);

        }
});

$.get("_getAllUserbase", function (data) {
        getAllUserbase = data

        var sel = $('#layergroupselect')[0]
        createList(sel)
})
function geotifflayer() {
        var wms_source = new ol.source.TileWMS({
                url: hostUrl + '/Notel/wms',
                params: {
                        'LAYERS': 'Notel:geotiff',

                },
                crossOrigin: 'anonymous'
        });
        var wms_layer = new ol.layer.Tile({
                source: wms_source
        });
        layerName = 'geotiff'
        wms_layer.set('name', 'geotiff')

        myMap.addLayer(wms_layer)
        infocomingfromlist = false
        layertolegend()

        zoomtogeotiff()


}

function zoomtogeotiff() {
        var url = hostUrl + '/wms?request=GetCapabilities&service=WMS&version=1.1.0';
        var parser = new ol.format.WMSCapabilities();


        $.ajax(url).then(function (response) {
                var result = parser.read(response);
                var Layers = result.Capability.Layer.Layer;
                var extent;
                for (var i = 0; i < Layers.length; i++) {
                        var layerobj = Layers[i];
                        if (layerobj.Name == "Notel:geotiff") {

                                extent = layerobj.BoundingBox[0].extent;
                                // var scr = new ol.proj.Projection('EPSG:32634');
                                // var dest = new ol.proj.Projection('EPSG:3857');
                                // var ex = ol.proj.transform(extent, 'EPSG:32634', 'EPSG:3857');
                                var firstProjection = '+proj=merc +a=6378137 +b=6378137 +lat_ts=0.0 +lon_0=0.0 +x_0=0.0 +y_0=0 +k=1.0 +units=m +nadgrids=@null +wktext  +no_defs';
                                var secondProjection = "+proj=utm +zone=34 +datum=WGS84 +units=m +no_defs";
                                //I'm not going to redefine those two in latter examples.
                                var ex1 = proj4(secondProjection, firstProjection, [extent[0], extent[1]]);
                                var ex2 = proj4(secondProjection, firstProjection, [extent[2], extent[3]]);
                                var ex = []
                                ex.push(ex1[0], ex1[1])
                                ex.push(ex2[0], ex2[1])
                        }

                }
                // myMap.getView().fit(ex, myMap.getSize());
        });
}
//zoom to first country 
var allco = $('#allbasicinfocountry')[0].innerHTML.split(',')
if (allco[0] != '' && allco[0] != 'None') {
        var onecountry = (allco[0])
        var vectorSource = new ol.source.Vector();
        var vectorLayer = new ol.layer.Vector({
                source: vectorSource,
                style: new ol.style.Style({
                        stroke: new ol.style.Stroke({
                                color: 'rgba(0, 0, 255, 0)',
                                width: 1
                        })
                })
        });
        // generate a GetFeature request
        var featureRequest = new ol.format.WFS().writeGetFeature({
                srsName: 'EPSG:3857',
                featureNS: hostUrl + '/Notel/ows?service=WFS',
                featurePrefix: 'Notel',
                featureTypes: ['allcountries'],
                outputFormat: 'application/json',
                filter:

                        ol.format.filter.like('mccno', "*" + onecountry + "*")

        });

        // then post the request and add the received features to a layer
        fetch(hostUrl + '/Notel/ows?service=WFS', {
                method: 'POST',
                body: new XMLSerializer().serializeToString(featureRequest)
        }).then(function (response) {
                return response.json();
        }).then(function (json) {
                var features = new ol.format.GeoJSON().readFeatures(json);
                vectorSource.addFeatures(features);
                // myMap.getView().fit(vectorSource.getExtent());
        });
}
//Country based on person's privilage
privilagedcountry()
function privilagedcountry() {

        if (allco[0] != "" && allco[0] != 'None') {
                cqlfilter = "mccno like '%"
                for (i = 0; i < allco.length; i++) {
                        if (i < 1) {
                                cqlfilter = cqlfilter + parseInt(allco[i]) + "%'"
                        } else {
                                cqlfilter = cqlfilter + " or mccno like '%" + parseInt(allco[i]) + "%'"
                        }
                }

                var wms_source = new ol.source.TileWMS({
                        url: hostUrl + '/Notel/wms',
                        params: {
                                'LAYERS': 'Notel:allcountries',
                                'CQL_FILTER': cqlfilter
                        },
                        crossOrigin: 'anonymous'
                });
        } else {
                var wms_source = new ol.source.TileWMS({
                        url: hostUrl + '/Notel/wms',
                        params: {
                                'LAYERS': 'Notel:allcountries',

                        },
                        crossOrigin: 'anonymous'
                });
        }
        var wms_layer = new ol.layer.Tile({
                source: wms_source
        });
        wms_layer.set('name', 'country')

        myMap.addLayer(wms_layer)



}



function createList(sel) {
        var selOption = sel.options[sel.selectedIndex].value;
        // $('#createlegendoflayers')[0].innerHTML = ''
        // if(selOption=="all"){
        //         sqlString = 'select * from app_layerlistdata'
        // }
        // else  if(selOption=="rfbenchmark"){
        //         sqlString = 'select * from app_layerlistdata where setname = %s'
        // }
        // else  if(selOption=="tems"){
        //         sqlString = 'select * from app_layerlistdata where setname = %s'
        // }


        if (selOption == 'amdin') {
                $("#layerlistdiv").html("")
                $("#layersublistdiv").html("")
                curHtml = '';
                curHtml = $("#layersublistdiv").html();
                divstring = '<div class="subListItem"><label class="kt-checkbox" ><input type="checkbox" checked onclick="showcountryOnMap(this)" value="country" id="check-country" name="country">Allocated country boundary<span></span></label></div>'



                curHtml += divstring
                $("#layersublistdiv").html(curHtml)
                curHtml = '';
                curHtml = $("#layersublistdiv").html()
                divstring = '<div class="subListItem"><label class="kt-checkbox" ><input type="checkbox"  onclick="showtiff(this)" value="geotiff" id="check-geotiff" name="Geotiff"">Geotiff<span></span></label></div>'
                curHtml += divstring


                $("#layersublistdiv").html(curHtml)
                checkpresentlayer()


        }
        else if (selOption == 'query') {
                $("#layerlistdiv").html("")
                $("#layersublistdiv").html("")
                addlayertolist()

        }
        /* else if(selOption=='active'){
                 $.getJSON('/getLayerOperators/',{'layer':'active_test_upload'},function(data){
                         geoBinOperators = data;
                         console.log(geoBinOperators);
                         var div = document.getElementById('operatorselect');
                                 while(div.hasChildNodes())
                                 {
                                      div.removeChild(div.firstChild);
                                 }
                         for (i = 0; i < geoBinOperators.length; i++) {
                               var option = document.createElement("option");
                               option.value = geoBinOperators[i].values;
                               // option.label = farr[d];
                               option.innerText = geoBinOperators[i].values;
                               div.appendChild(option);
                         }
                         // selectElement.innerHTML = div.innerHTML
                       });
         }   */
        else {

                $.get('layerlistinformation',

                        function (data) {

                                $("#layerlistdiv").html("");
                                $("#layersublistdiv").html("");
                                // $("#layersublistdivFilter").html("");

                                layerdata = data

                                divstring = '<div class="listItem">' + selOption + '<span></span></div>'
                                $("#layerlistdiv").html(divstring)

                                curHtml = $("#layerlistdiv").html()

                                curHtml += '<div class="subList" id="sub1">'

                                for (i = 0; i < getAllUserbase.length; i++) {
                                        var username = $('#allbasicinfousername')[0].innerText
                                        if (username == getAllUserbase[i].username) {
                                                // if (getAllUserbase[i].is_admin == true) {
                                                //         var allocatedlayers = []
                                                //         for (j = 0; j < layerdata.length; j++) {
                                                //                 allocatedlayers.push(String(layerdata[j].id))
                                                //         }
                                                // } else {
                                                var allocatedlayers = document.getElementById('allbasicinfolayers').innerText.split(',')
                                                if (allocatedlayers[0] == 'None' || allocatedlayers[0] == '') {
                                                        allocatedlayers = []
                                                        for (j = 0; j < layerdata.length; j++) {
                                                                allocatedlayers.push(String(layerdata[j].id))
                                                                allocatedlayenames.push(layerdata[j].layername)
                                                        }
                                                } else {
                                                        allocatedlayenames
                                                        for (f = 0; f < layerdata.length; f++) {
                                                                if (allocatedlayers.includes(String(layerdata[f].id))) {
                                                                        allocatedlayenames.push(layerdata[f].layername)
                                                                }
                                                        }
                                                }
                                                var allassignedoperators = document.getElementById('allbasicinfooperator').innerText.split(',')
                                                var selectdrop = $('#operatorselect')[0]
                                                if (allassignedoperators[0] == 'None' || allassignedoperators[0] == '') {

                                                        allassignedoperators = []
                                                        if (alloperatorslistforlayerlist == undefined) {
                                                                swal.fire("Something went wrong, Please refresh the page!", "", "error");

                                                        } else {

                                                                allOpList = []
                                                                $('#operatorselect').empty();
                                                                for (j = 0; j < alloperatorslistforlayerlist.length; j++) {
                                                                        var op = document.createElement('option')
                                                                        op.value = alloperatorslistforlayerlist[j]["operatorcode"]
                                                                        op.innerHTML = alloperatorslistforlayerlist[j]["name"] + " - " + alloperatorslistforlayerlist[j]["operatorcode"];
                                                                        selectdrop.appendChild(op)
                                                                        allOpList.push(op)
                                                                }


                                                                var sortSelect = function (select, attr, order) {
                                                                        if (attr === 'text') {
                                                                                if (order === 'asc') {
                                                                                        $(select).html($(select).children('option').sort(function (x, y) {
                                                                                                return $(x).text().toUpperCase() < $(y).text().toUpperCase() ? -1 : 1;
                                                                                        }));
                                                                                        $(select).get(0).selectedIndex = 0;

                                                                                }// end asc
                                                                                if (order === 'desc') {
                                                                                        $(select).html($(select).children('option').sort(function (y, x) {
                                                                                                return $(x).text().toUpperCase() < $(y).text().toUpperCase() ? -1 : 1;
                                                                                        }));
                                                                                        $(select).get(0).selectedIndex = 0;

                                                                                }// end desc
                                                                        }

                                                                };

                                                                sortSelect('#operatorselect', 'text', 'asc');


                                                                // $("#operatorselect option").each(function()
                                                                // {
                                                                //         allOpList.push($(this).val())        
                                                                // });
                                                                $('#operatorselect').find('option:selected').prop("selected", false)


                                                        }
                                                } else {
                                                        $('#operatorselect').empty();
                                                        for (j = 0; j < allassignedoperators.length; j++) {
                                                                var op = document.createElement('option')
                                                                op.value = allassignedoperators[j]
                                                                op.innerHTML = allassignedoperators[j]
                                                                selectdrop.appendChild(op)
                                                        }
                                                }
                                                // }
                                        }
                                }

                                $("#layerlistdiv").html(divstring)
                                for (len = 0; len < layerdata.length; len++) {
                                        if (allocatedlayers.includes(String(layerdata[len].id))) {
                                                $('#' + layerdata[len].setname)[0].style.display = 'block'
                                                // $('#' + layerdata[len].setname)[0].selected = true
                                                if (selOption == layerdata[len].setname) {

                                                        curHtml = '';
                                                        curHtml = $("#layersublistdiv").html()
                                                        var mainDivforlist = document.createElement('div')
                                                        mainDivforlist.className = 'subListItem'

                                                        curHtml_ = '';
                                                        curHtml_ = $("#layersublistdiv").html()
                                                        var mainDivforlist_ = document.createElement('div')
                                                        mainDivforlist_.className = 'subListItem'


                                                        var contDivforlist = document.createElement('div')
                                                        var headcontDivforlist = document.createElement('div')
                                                        var intcontDivforlist = document.createElement('div')
                                                        intcontDivforlist.setAttribute('class', "internaldiv")
                                                        intcontDivforlist.setAttribute('id', "internaldiv-" + len)
                                                        var firstdiv = document.createElement('div')
                                                        var imagewrapperdiv = document.createElement('div')
                                                        var tagwrapperdiv = document.createElement('div')
                                                        tagwrapperdiv.setAttribute('class', "taglabel")




                                                        var buttonforopeningdiv = document.createElement('i')
                                                        buttonforopeningdiv.setAttribute('class', "la la-angle-double-right")
                                                        buttonforopeningdiv.setAttribute('id', "" + len)
                                                        buttonforopeningdiv.setAttribute('onclick', 'addClassForOpeningDiv(this)')
                                                        // buttonforopeningdiv.setAttribute('onclick', 'showdiv()')
                                                        var legendimg = document.createElement('img')
                                                        if (layerdata[len]["layername"].startsWith('bin_size')) {
                                                                legendimg.setAttribute('src', layergroup)
                                                                legendimg.style.width = '20px'

                                                        }
                                                        else {
                                                                if (layerdata[len]["layername"] == 'rfsig_meas_raw_geom') {
                                                                        legendimg.setAttribute('src', hostUrl + "/wms?REQUEST=GetLegendGraphic&VERSION=1.0.0&FORMAT=image/png&WIDTH=10&HEIGHT=10&LAYER=Notel:" + layerdata[len]["layername"] + "&legend_options=layout:horizontal;rows:3;fontName:Times%20New%20Roman;fontAntiAliasing:true;fontColor:0xffffff;fontSize:6;bgColor:0x00FFFF00;dpi:180&TRANSPARENT=true&SCALE=10000")

                                                                } else if (layerdata[len]["layername"] == 'rfsig_tests_hexabins') {
                                                                        if (layerdata[len]["alias_name"] == "Tests Hexabins latency") {
                                                                                legendimg.setAttribute('src', hostUrl + "/wms?REQUEST=GetLegendGraphic&VERSION=1.0.0&FORMAT=image/png&WIDTH=10&HEIGHT=10&LAYER=Notel:" + layerdata[len]["layername"] + "&STYLE=rfsig_tests_hexabins_latency&legend_options=layout:horizontal;fontName:Times%20New%20Roman;fontAntiAliasing:true;fontColor:0xffffff;fontSize:6;bgColor:0xFFFFEE;dpi:180&TRANSPARENT=true&SCALE=8735660")

                                                                        } else {
                                                                                legendimg.setAttribute('src', hostUrl + "/wms?REQUEST=GetLegendGraphic&VERSION=1.0.0&FORMAT=image/png&WIDTH=10&HEIGHT=10&LAYER=Notel:" + layerdata[len]["layername"] + "&legend_options=layout:horizontal;fontName:Times%20New%20Roman;fontAntiAliasing:true;fontColor:0xffffff;fontSize:6;bgColor:0xFFFFEE;dpi:180&TRANSPARENT=true&SCALE=8735660")

                                                                        }

                                                                } else {
                                                                        legendimg.setAttribute('src', hostUrl + "/wms?REQUEST=GetLegendGraphic&VERSION=1.0.0&FORMAT=image/png&WIDTH=10&HEIGHT=10&LAYER=Notel:" + layerdata[len]["layername"] + "&legend_options=layout:horizontal;fontName:Times%20New%20Roman;fontAntiAliasing:true;fontColor:0xffffff;fontSize:6;bgColor:0xFFFFEE;dpi:180&TRANSPARENT=true")

                                                                }
                                                                // mainDiv.appendChild(legendimg)
                                                        }




                                                        var labelformainDiv = document.createElement('label')

                                                        labelformainDiv.className = "kt-checkbox"
                                                        var input = document.createElement('input')
                                                        input.setAttribute('type', "checkbox")
                                                        labelformainDiv.innerText = layerdata[len]["alias_name"]
                                                        input.setAttribute('onclick', 'showLayerOnMap(this)')
                                                        input.setAttribute('value', layerdata[len]["wms"])
                                                        input.setAttribute('id', "check-" + layerdata[len]["layername"])
                                                        input.setAttribute('name', layerdata[len]["layername"])
                                                        input.setAttribute('zindex', len)
                                                        labelformainDiv.appendChild(input)

                                                        var labelmaindivspan = document.createElement('span')
                                                        labelformainDiv.appendChild(labelmaindivspan)

                                                        /*var labelformainDiv_ = document.createElement('label')

                                                        labelformainDiv_.className = "kt-checkbox"
                                                        var input_ = document.createElement('input')
                                                        input_.setAttribute('type', "checkbox")
                                                        labelformainDiv_.innerText = layerdata[len]["alias_name"]
                                                        input_.setAttribute('onclick', 'getGeoBins("'+layerdata[len]["layername"]+'",this)')
                                                        input_.setAttribute('value', layerdata[len]["wms"])
                                                        input_.setAttribute('id', "check--" + layerdata[len]["layername"])
                                                        input_.setAttribute('name', '_'+layerdata[len]["layername"])
                                                        input_.setAttribute('zindex', len)
                                                        labelformainDiv_.appendChild(input_)

                                                        var labelmaindivspan_ = document.createElement('span');
                                                        labelformainDiv_.appendChild(labelmaindivspan_);
                                                        mainDivforlist_.appendChild(labelformainDiv_);
                                                        $('#layersublistdivFilter')[0].appendChild(mainDivforlist_);*/

                                                        var sliderspacediv = document.createElement('div')
                                                        sliderspacediv.className = 'w-100'
                                                        var sliderdiv = document.createElement('div')
                                                        sliderdiv.className = 'options'
                                                        var sliderinput = document.createElement('input')
                                                        sliderinput.setAttribute('type', "range")
                                                        sliderinput.setAttribute('class', "custom-range")
                                                        sliderinput.setAttribute('onchange', "changeLayerOpac(this)")
                                                        sliderinput.setAttribute('min', "0")
                                                        sliderinput.setAttribute('max', "100")
                                                        sliderinput.setAttribute('value', "70")
                                                        sliderinput.setAttribute('id', layerdata[len]["layername"])

                                                        var labelcheckbox = document.createElement('input')
                                                        labelcheckbox.setAttribute('type', "checkbox")
                                                        labelcheckbox.setAttribute('onclick', "Labelonoff(this)")
                                                        labelcheckbox.setAttribute('id', "label-" + layerdata[len]["layername"])
                                                        var labelspan = document.createElement('span')
                                                        labelspan.text = 'label'
                                                        var labellabel = document.createElement('label')
                                                        labellabel.appendChild(labelcheckbox)
                                                        labellabel.appendChild(labelspan)
                                                        var labelspanmain = document.createElement('span')
                                                        labelspanmain.className = "kt-switch kt-switch--dark kt-switch--sm"
                                                        labelspanmain.appendChild(labellabel)

                                                        var labeltext = document.createElement('span')
                                                        labeltext.innerText = "label"

                                                        var buttonforlabeldiv = document.createElement('i')
                                                        buttonforlabeldiv.setAttribute('class', "la la-tag")
                                                        buttonforlabeldiv.setAttribute('onclick', "Labelonoff(this)")
                                                        buttonforlabeldiv.setAttribute('id', "label-" + layerdata[len]["layername"])


                                                        tagwrapperdiv.appendChild(buttonforlabeldiv)
                                                        imagewrapperdiv.appendChild(legendimg.cloneNode(true))
                                                        // dropdown for cell modified
                                                        for (b = 0; b < dropdownlayers.length; b++) {
                                                                if (layerdata[len]["layername"] == dropdownlayers[b]) {
                                                                        var dropdownforcells = document.createElement('select')
                                                                        dropdownforcells.setAttribute('onchange', "changecelldate(this)")
                                                                        dropdownforcells.id = 'selectof' + layerdata[len]["layername"]
                                                                        var allUniqueDates = []
                                                                        $.getJSON(hostUrl + '/ows?service=WFS&version=1.0.0&request=GetFeature&typeName=Notel%3Acell_modified_freqn&PROPERTYNAME=created_time&outputformat=application/json', function (data) {
                                                                                console.log(data)
                                                                                var allData = data.features
                                                                                for (d = 0; d < allData.length; d++) {
                                                                                        var theDate = allData[d].properties.created_time.split("T")[0]
                                                                                        if (allUniqueDates.includes(theDate)) {

                                                                                        } else {
                                                                                                allUniqueDates.push(theDate)
                                                                                        }
                                                                                }
                                                                                allUniqueDates.sort(function (a, b) {
                                                                                        var da = new Date(a).getTime();
                                                                                        var db = new Date(b).getTime();

                                                                                        return da < db ? -1 : da > db ? 1 : 0
                                                                                });

                                                                                for (s = 0; s < 3; s++) {
                                                                                        if (allUniqueDates[s]) {
                                                                                                var op = document.createElement('option')
                                                                                                op.value = allUniqueDates[s];
                                                                                                op.innerHTML = allUniqueDates[s];
                                                                                                dropdownforcells.append(op)
                                                                                        }
                                                                                }
                                                                                imagewrapperdiv.appendChild(dropdownforcells.cloneNode(true))

                                                                        })
                                                                }
                                                        }

                                                        sliderdiv.appendChild(imagewrapperdiv)
                                                        sliderdiv.appendChild(sliderinput)

                                                        firstdiv.appendChild(buttonforopeningdiv.cloneNode(true))
                                                        //firstdiv.appendChild(labeltext)
                                                        firstdiv.appendChild(labelformainDiv.cloneNode(true))
                                                        firstdiv.appendChild(tagwrapperdiv)


                                                        headcontDivforlist.appendChild(firstdiv)
                                                        intcontDivforlist.appendChild(sliderdiv)

                                                        // mainDivforlist.appendChild(firstdiv)
                                                        // mainDivforlist.appendChild(sliderdiv)

                                                        contDivforlist.appendChild(headcontDivforlist)
                                                        contDivforlist.appendChild(intcontDivforlist)

                                                        mainDivforlist.appendChild(contDivforlist)

                                                        $('#layersublistdiv')[0].appendChild(mainDivforlist)

                                                }
                                        }
                                }


                                checkpresentlayer()

                        });
        }

        function checkpresentlayer() {
                var layersArray = myMap.getLayers().getArray();
                for (var i = 0; i < layersArray.length; i++) {
                        var layename = layersArray[i].getProperties().name
                        if (layename != undefined) {
                                if (layename.startsWith('servingbaselayer')) {
                                } else {
                                        if ($('#check-' + layename)[0]) {
                                                $('#check-' + layename)[0].checked = true
                                                if (layersArray[i].getSource().params_.styles != undefined) {
                                                        if (layersArray[i].getSource().params_.STYLES.split('_')[layersArray[i].getSource().params_.STYLES.split('_').length - 1] == 'label') {
                                                                $('#check-' + layename)[0].parentElement.parentElement.getElementsByClassName('taglabel')[0].style.backgroundColor = 'rgb(255, 184, 34,1)'
                                                        }
                                                }
                                        }
                                }
                        }
                }
        }
}


function showtiff(checkbox) {
        var layersArray = myMap.getLayers().getArray();
        for (var i = 0; i < layersArray.length; i++) {

                if (layersArray[i].getProperties().name == 'geotiff') {
                        layer = layersArray[i];
                        break;
                }

        }
        if (checkbox.checked == true) {
                geotifflayer()
        } else {
                myMap.removeLayer(layer)
        }

}

function addlayertolist() {
        myMap.getLayers().forEach(
                function (layer) {
                        if (layer != undefined) {
                                var name = layer.get('name');
                                if (name === 'attributeResult') {

                                        curHtml = '';
                                        curHtml = $("#layersublistdiv").html();
                                        divstring = '<div class="subListItem"><label class="kt-checkbox" ><input type="checkbox" id="attributeResultx" checked onclick="TogglequeryLayer(this)" value="querylayer"  name="querylayer">Attribute Query Layer<span></span></label></div>'
                                        curHtml += divstring
                                        $("#layersublistdiv").html(curHtml)

                                }
                        }

                });
        myMap.getLayers().forEach(
                function (layer) {
                        if (layer != undefined) {
                                var name = layer.get('name');
                                if (name === 'spatialResult') {

                                        curHtml = '';
                                        curHtml = $("#layersublistdiv").html();
                                        divstring = '<div class="subListItem"><label class="kt-checkbox" ><input type="checkbox" id="spatialResultx" checked onclick="TogglequeryLayer(this)" value="querylayer"  name="querylayer">Spatial Query Layer<span></span></label></div>'
                                        curHtml += divstring
                                        $("#layersublistdiv").html(curHtml)

                                }
                        }

                });
        myMap.getLayers().forEach(
                function (layer) {
                        if (layer != undefined) {
                                var name = layer.get('name');
                                if (name === 'CSVlayer') {

                                        curHtml = '';
                                        curHtml = $("#layersublistdiv").html();
                                        divstring = '<div class="subListItem"><label class="kt-checkbox" ><input type="checkbox" id="CSVlayerx" checked onclick="TogglequeryLayer(this)" value="querylayer"  name="querylayer">CSV Layer<span></span></label></div>'
                                        curHtml += divstring
                                        $("#layersublistdiv").html(curHtml)

                                }
                        }

                });
        myMap.getLayers().forEach(
                function (layer) {
                        if (layer != undefined) {
                                var name = layer.get('name');
                                if (name === 'filterbycell') {

                                        curHtml = '';
                                        curHtml = $("#layersublistdiv").html();
                                        divstring = '<div class="subListItem"><label class="kt-checkbox" ><input type="checkbox" id="filterbycellx" checked onclick="TogglequeryLayer(this)" value="querylayer"  name="querylayer">Filter by selected cell<span></span></label></div>'
                                        curHtml += divstring
                                        $("#layersublistdiv").html(curHtml)

                                }
                        }

                });
        myMap.getLayers().forEach(
                function (layer) {
                        if (layer != undefined) {
                                var name = layer.get('name');
                                if (name === 'layerlines') {

                                        curHtml = '';
                                        curHtml = $("#layersublistdiv").html();
                                        divstring = '<div class="subListItem"><label class="kt-checkbox" ><input type="checkbox" id="layerlinesx" checked onclick="TogglequeryLayer(this)" value="querylayer"  name="querylayer">Serving Lines<span></span></label></div>'
                                        curHtml += divstring
                                        $("#layersublistdiv").html(curHtml)

                                }
                        }

                });
        myMap.getLayers().forEach(
                function (layer) {
                        if (layer != undefined) {
                                var name = layer.get('name');
                                if (name === 'ColorCoverage') {

                                        curHtml = '';
                                        curHtml = $("#layersublistdiv").html();
                                        divstring = '<div class="subListItem"><label class="kt-checkbox" ><input type="checkbox" id="ColorCoveragex" checked onclick="TogglequeryLayer(this)" value="querylayer"  name="querylayer">Color Coverage<span></span></label></div>'
                                        curHtml += divstring
                                        $("#layersublistdiv").html(curHtml)

                                }
                        }

                });
}
function showcountryOnMap(checkbox) {
        var layersArray = myMap.getLayers().getArray();
        for (var i = 0; i < layersArray.length; i++) {

                if (layersArray[i].getProperties().name == 'country') {
                        layer = layersArray[i];
                        break;
                }

        }


        if (checkbox.checked == true) {
                privilagedcountry()
        } else {
                myMap.removeLayer(layer)
        }
}

function TogglequeryLayer(val) {
        myMap.getLayers().forEach(
                function (layer) {
                        if (layer != undefined) {
                                var name = layer.get('name');
                                if (name === val.id.slice(0, -1)) {
                                        var opac = layer.getOpacity()
                                        if (opac == 1) {
                                                layer.setOpacity(0)
                                        } else {
                                                layer.setOpacity(1)
                                        }
                                }
                        }

                });

}


//update the cell modified according to date 
function changecelldate(date) {
        var LayChecked = date.parentElement.parentElement.parentElement.parentElement.getElementsByTagName('input')[0].checked
        var selectedDate = date.value
        if (LayChecked) {
                var layerNamee = date.parentElement.parentElement.parentElement.parentElement.getElementsByTagName('input')[0].name
                myMap.getLayers().forEach(
                        function (layer) {
                                if (layer != undefined) {
                                        var name = layer.get('name');
                                        if (name === layerNamee) {
                                                layer.getSource().updateParams({ CQL_FILTER: 'created_time > ' + selectedDate })
                                        }
                                }

                        });
        }
}

function Labelonoff(labelon_off_String) {

        // if (labelon_off_String.parentElement.parentElement.parentElement.parentElement.getElementsByTagName('input')[0].checked) {
        if ($('.listItem')[0].innerText == 'bin') {
                Swal.fire({
                        title: "Labels do not work on Bin Layer!",
                        type: 'error',
                        timer: 2000,
                        position: 'top-end',
                        showConfirmButton: false

                })
        } else {
                document.getElementById('load').style.visibility = "visible";

                if (labelon_off_String.parentElement.parentElement.getElementsByClassName('kt-checkbox')[0].firstElementChild.checked) {
                        $("body").css("cursor", "progress");
                        layId = labelon_off_String.id.split('-')[1]
                        layeropacvaltext = labelon_off_String.parentElement.parentElement.parentElement.parentElement.getElementsByClassName('custom-range')[0].value
                        layeropacval = parseInt(layeropacvaltext) / 100
                        curLay = getLayerFromMap(layId)
                        if (curLay != null) {
                                myMap.removeLayer(curLay)
                        }
                        var stylename
                        if (labelon_off_String.classList.contains("showlabel") != true) {
                                labelon_off_String.parentElement.style.backgroundColor = 'rgb(255, 184, 34,1)'
                                stylename = layId + '_label'
                                labelon_off_String.classList.add("showlabel")
                        } else {
                                labelon_off_String.parentElement.style.backgroundColor = "rgba(0, 0, 0, 0.5)"
                                stylename = layId
                                labelon_off_String.classList.remove("showlabel");
                        }
                        cqlfilter = '1=1'
                        if (document.getElementById('advanceopinlayerlist').style.display == 'block') {
                                for (i = 0; i < layerdata.length; i++) {
                                        if (layId == layerdata[i].layername) {
                                                var datecol = layerdata[i].datecol
                                                if (datecol == "") {
                                                        datecol = null
                                                }
                                                var timefrom = $('#timefrom')[0].value
                                                var timetill = $('#timetill')[0].value
                                                var operatorcol = layerdata[i].operatorcol
                                                if (operatorcol == "") {
                                                        operatorcol = null
                                                }
                                                var allop = $('#operatorselect').val()
                                                var techcol = layerdata[i].techcol
                                                if (techcol == "") {
                                                        techcol = null
                                                }
                                                var alltech = $('#technologyselect').val()


                                                if (datecol != null) {
                                                        if (timefrom == "" && timetill == '') {

                                                        } else {
                                                                if (timefrom == "") {
                                                                        cqlfilter += " and " + datecol + " <= '" + timetill + "'"

                                                                } else if (timetill == '') {
                                                                        cqlfilter += " and " + datecol + " >= '" + timefrom + "'"
                                                                } else if (timetill == timefrom) {
                                                                        var datetimetill = new Date(timetill)
                                                                        datetimetill.setDate(datetimetill.getDate() + 1)

                                                                        var temptimetill = datetimetill.toISOString().substr(0, 10);
                                                                        cqlfilter += " and " + datecol + " >= '" + timefrom + "' and " + datecol + " <= '" + temptimetill + "'"
                                                                } else {
                                                                        cqlfilter += " and " + datecol + " >= '" + timefrom + "' and " + datecol + " <= '" + timetill + "'"
                                                                }
                                                        }
                                                }
                                                if (operatorcol != null) {
                                                        if (allop[0] != "None" && allop[0] != "" && allop[0] != undefined) {
                                                                cqlfilter += " and " + operatorcol + " in ("
                                                                for (i = 0; i < allop.length; i++) {
                                                                        if (i < 1) {
                                                                                cqlfilter += parseInt(allop[i])
                                                                        } else {
                                                                                cqlfilter += "," + parseInt(allop[i])
                                                                        }
                                                                }
                                                                cqlfilter += ")"
                                                        }
                                                        //cqlfilter += " " + datecol + " > '" + timefrom + "' and " + datecol + " < '" + timetill + "'"
                                                }
                                                if (techcol != null) {
                                                        if (alltech[0] != "None" && alltech[0] != "" && alltech[0] != undefined) {
                                                                cqlfilter += " and " + techcol + " in ("
                                                                for (i = 0; i < alltech.length; i++) {
                                                                        if (i < 1) {
                                                                                cqlfilter += "'" + alltech[i] + "'"
                                                                        } else {
                                                                                cqlfilter += ",'" + alltech[i] + "'"
                                                                        }
                                                                }
                                                                cqlfilter += ")"
                                                        }
                                                        //cqlfilter += " " + datecol + " > '" + timefrom + "' and " + datecol + " < '" + timetill + "'"
                                                }
                                        }


                                }
                        } else {

                        }
                        // if (techcol == null && operatorcol == null) {
                        //         $('#infoaboutselectedlayertext')[0].innerText = 'No information about operators and technology is available'
                        // } else if (techcol == null) {
                        //         $('#infoaboutselectedlayertext')[0].innerText = 'No information about technology is available'

                        // } else if (operatorcol == null) {
                        //         $('#infoaboutselectedlayertext')[0].innerText = 'No information about operators is available'
                        // } else {
                        //         $('#infoaboutselectedlayertext')[0].innerText = 'Please wait while layer is loading'
                        // }

                        if (layId == 'bin_size') {
                                sources = new ol.source.ImageWMS({
                                        visible: true,
                                        ratio: 1,
                                        url: hostUrl + '/Notel/wms',
                                        params: {
                                                'FORMAT': 'image/png',
                                                'VERSION': '1.1.0',
                                                "LAYERS": 'Notel:' + layId,
                                                'CQL_FILTER': cqlfilter,
                                                "exceptions": 'application/vnd.ogc.se_inimage'
                                        },
                                        crossOrigin: 'anonymous',

                                });

                        } else if (layerName == 'rfsig_tests_hexabins') {

                                sources = new ol.source.ImageWMS({
                                        visible: true,
                                        ratio: 1,
                                        url: hostUrl + '/Notel/wms',
                                        params: {
                                                'FORMAT': 'image/png',
                                                'VERSION': '1.1.0',
                                                "LAYERS": 'Notel:' + layerName,
                                                // 'CQL_FILTER': cqlfilter,
                                                "exceptions": 'application/vnd.ogc.se_inimage'
                                        },
                                        crossOrigin: 'anonymous',

                                });


                        }
                        else if (dropdownlayers.includes(layerName)) {
                                cqlfilter = '1=1'
                                if (operatorcol != null) {
                                        if (allop[0] != "None" && allop[0] != "" && allop[0] != undefined) {
                                                cqlfilter += " and " + operatorcol + " in ("
                                                for (i = 0; i < allop.length; i++) {
                                                        if (i < 1) {
                                                                cqlfilter += parseInt(allop[i])
                                                        } else {
                                                                cqlfilter += "," + parseInt(allop[i])
                                                        }
                                                }
                                                cqlfilter += ")"
                                        }
                                        //cqlfilter += " " + datecol + " > '" + timefrom + "' and " + datecol + " < '" + timetill + "'"
                                }
                                if (techcol != null) {
                                        if (alltech[0] != "None" && alltech[0] != "" && alltech[0] != undefined) {
                                                cqlfilter += " and " + techcol + " in ("
                                                for (i = 0; i < alltech.length; i++) {
                                                        if (i < 1) {
                                                                cqlfilter += "'" + alltech[i] + "'"
                                                        } else {
                                                                cqlfilter += ",'" + alltech[i] + "'"
                                                        }
                                                }
                                                cqlfilter += ")"
                                        }
                                        //cqlfilter += " " + datecol + " > '" + timefrom + "' and " + datecol + " < '" + timetill + "'"
                                }
                                cqlfilter += 'and created_time > ' + document.getElementById('selectof' + layerName + '').value
                                sources = new ol.source.ImageWMS({
                                        visible: true,
                                        ratio: 1,
                                        url: hostUrl + '/Notel/wms',
                                        params: {
                                                'FORMAT': 'image/png',
                                                'VERSION': '1.1.0',
                                                "LAYERS": 'Notel:' + layerName,
                                                'CQL_FILTER': cqlfilter,
                                                'STYLES': stylename,
                                                "exceptions": 'application/vnd.ogc.se_inimage'
                                        },
                                        crossOrigin: 'anonymous',

                                });


                        }
                        else {
                                sources = new ol.source.ImageWMS({
                                        visible: true,
                                        ratio: 1,
                                        url: hostUrl + '/Notel/wms',
                                        params: {
                                                'FORMAT': 'image/png',
                                                'VERSION': '1.1.0',
                                                "LAYERS": 'Notel:' + layId,
                                                "singleTile": false,
                                                'STYLES': stylename,
                                                'CQL_FILTER': cqlfilter,
                                                "exceptions": 'application/vnd.ogc.se_inimage'
                                        },
                                        crossOrigin: 'anonymous',

                                });
                        }



                        untiled = new ol.layer.Image({
                                source: sources,
                                minResolution: 0.00001,
                                maxResolution: 50000
                        });
                        untiled.set('name', layId);
                        untiled.setOpacity(layeropacval)
                        myMap.addLayer(untiled);
                        $("body").css("cursor", "default");
                        document.getElementById('load').style.visibility = "hidden";


                }
                else {
                        Swal.fire({
                                title: "Please enable layer first!",
                                type: 'error',
                                timer: 2000,
                                position: 'top-end',
                                showConfirmButton: false

                        })
                        document.getElementById('load').style.visibility = "hidden";

                }
        }

}


// } else {
//         swal.fire("Please enable layer first", "", "error");

// }


function changeLayerOpac(sliderItem) {

        layId = sliderItem.id
        opacVal = (sliderItem.value / 100)

        curLay = getLayerFromMap(layId)


        if (curLay != null) {
                curLay.setOpacity(opacVal)
        }

}

function layertolegend() {

        //add layer to legendlist 

        var imgDivforlegend = document.createElement('div')
        imgDivforlegend.className = 'imglegend'

        var mainDivforlegend = document.createElement('div')
        mainDivforlegend.className = 'checkbox'
        mainDivforlegend.id = 'legendidfor:' + layerName
        if (infocomingfromlist == true) {
                var img = thecheckbox.parentElement.parentElement.parentElement.parentElement.parentElement.getElementsByTagName('img')[0]
                var labelformainDiv = document.createElement('label')
                labelformainDiv.innerText = thecheckbox.parentElement.innerText

        } else {
                if (layerName == 'attributeResult') {
                        color = 'red'
                } else if (layerName == 'spatialResult') {
                        color = 'green'
                } else if (layerName == 'geoitff') {
                        color = 'green'
                } else if (layerName.startsWith('cel=')) {

                        color = 'rgba(148, 0, 211,1)'
                } else if (layerName.startsWith('sec=')) {

                        color = 'rgba(75, 0, 130,1)'
                } else if (layerName.startsWith('sit=')) {
                        color = 'rgba(255, 0, 255,1)'
                } else if (layerName == 'layerlines') {
                        color = 'rgba(0, 123, 158)'
                } else if (layerName == 'Color Coverage') {
                        color = 'rgba(255, 0, 0)'
                } else if (layerName == 'highlightlayer') {
                        color = 'rgba(255, 0, 0)'
                } else if (layerName == 'gridbinlayer') {
                        color = 'rgba(6, 117, 170)'
                } else if (layerName == 'hexbinlayer') {
                        color = 'rgba(6, 117, 170)'
                } else if (layerName == 'serv_line_ext') {
                        color = 'rgba(0, 0, 0)'
                } else {
                        color = color_forCSVLayer
                }
                var labelformainDiv = document.createElement('label')

                //             labelformainDiv.appendChild(img)
                // 
                labelformainDiv.innerHTML = "<span style='background-color:" + color + ";'> </span> " + layerName
        }
        var legendspan = document.createElement('span')
        var takeupatag = document.createElement('a')
        takeupatag.setAttribute('onclick', 'takelegendup(this)')
        var i = document.createElement('i')
        i.setAttribute('class', "la la-arrow-up")
        takeupatag.appendChild(i)
        legendspan.appendChild(takeupatag)
        var takedownatag = document.createElement('a')
        takedownatag.setAttribute('onclick', 'takelegenddown(this)')
        var i1 = document.createElement('i')
        i1.setAttribute('class', "la la-arrow-down")
        takedownatag.appendChild(i1)
        legendspan.appendChild(takedownatag)
        var zoomatag = document.createElement('a')
        zoomatag.setAttribute('onclick', 'zoomtolayer(this)')
        var i2 = document.createElement('i')
        i2.setAttribute('class', "la la-arrow-right")
        zoomatag.appendChild(i2)
        legendspan.appendChild(zoomatag)
        if (infocomingfromlist == true) {
                imgDivforlegend.appendChild(img.cloneNode(true))
        }


        mainDivforlegend.appendChild(labelformainDiv)
        mainDivforlegend.appendChild(imgDivforlegend)
        mainDivforlegend.appendChild(legendspan)


        $('#createlegendoflayers')[0].appendChild(mainDivforlegend)
}

var turnedOnLayers = [];
function showLayerOnMap(checkbox) {
        $("body").css("cursor", "progress");
        // layerUrl = checkbox.value
        layerName = checkbox.name
        thecheckbox = checkbox
        layeropacvaltext = checkbox.parentElement.parentElement.parentElement.parentElement.getElementsByClassName('custom-range')[0].value
        layeropacval = parseInt(layeropacvaltext) / 100
        // when checked show layer
        if (checkbox.checked == true) {
                if(!turnedOnLayers.includes(layerName)){
                        turnedOnLayers.push(layerName);
                }
                var stylename
                if (checkbox.parentElement.parentElement.getElementsByClassName('taglabel')[0].style.backgroundColor == 'rgb(255, 184, 34)') {
                        stylename = layerName + '_label'
                } else {
                        stylename = layerName
                }


                //   finalUrl = hostUrl + layerUrl;
                cqlfilter = '1=1'
                // If Advanced filter selected
                /*if (document.getElementById('advanceopinlayerlist').style.display == 'block') {

                        for (i = 0; i < layerdata.length; i++) {
                                if (layerName == layerdata[i].layername) {
                                        // if (document.getElementById('advanceopinlayerlist').style.display == 'block') {
                                        var datecol = layerdata[i].datecol
                                        if (datecol == "") {
                                                datecol = null
                                        }
                                        var timefrom = $('#timefrom')[0].value
                                        var timetill = $('#timetill')[0].value
                                        var operatorcol = layerdata[i].operatorcol
                                        if (operatorcol == "") {
                                                operatorcol = null
                                        }
                                        if ($('#selectallforop')[0].checked) {
                                                var allop = ''
                                        } else {
                                                var allop = $('#operatorselect').val()
                                        }

                                        var techcol = layerdata[i].techcol
                                        if (techcol == "") {
                                                techcol = null
                                        }
                                        if ($('#selectallfortech')[0].checked) {
                                                var alltech = ''
                                        } else {
                                                var alltech = $('#technologyselect').val()
                                        }


                                        if (datecol != null) {
                                                if (timefrom == "" && timetill == '') {

                                                } else {
                                                        if (timefrom == "") {
                                                                cqlfilter += " and " + datecol + " <= '" + timetill + "'"

                                                        } else if (timetill == '') {
                                                                cqlfilter += " and " + datecol + " >= '" + timefrom + "'"
                                                        } else if (timetill == timefrom) {
                                                                var datetimetill = new Date(timetill)
                                                                datetimetill.setDate(datetimetill.getDate() + 1)

                                                                var temptimetill = datetimetill.toISOString().substr(0, 10);
                                                                cqlfilter += " and " + datecol + " >= '" + timefrom + "' and " + datecol + " <= '" + temptimetill + "'"
                                                        }
                                                        else {
                                                                cqlfilter += " and " + datecol + " >= '" + timefrom + "' and " + datecol + " <= '" + timetill + "'"
                                                        }
                                                }
                                        }
                                        if (operatorcol != null) {
                                                if (allop[0] != "None" && allop[0] != "" && allop[0] != undefined) {
                                                        cqlfilter += " and " + operatorcol + " in ("
                                                        for (i = 0; i < allop.length; i++) {
                                                                if (i < 1) {
                                                                        cqlfilter += parseInt(allop[i])
                                                                } else {
                                                                        cqlfilter += "," + parseInt(allop[i])
                                                                }
                                                        }
                                                        cqlfilter += ")"
                                                }
                                                //cqlfilter += " " + datecol + " > '" + timefrom + "' and " + datecol + " < '" + timetill + "'"
                                        }
                                        if (techcol != null) {
                                                if (alltech[0] != "None" && alltech[0] != "" && alltech[0] != undefined) {
                                                        cqlfilter += " and " + techcol + " in ("
                                                        for (i = 0; i < alltech.length; i++) {
                                                                if (i < 1) {
                                                                        cqlfilter += "'" + alltech[i] + "'"
                                                                } else {
                                                                        cqlfilter += ",'" + alltech[i] + "'"
                                                                }
                                                        }
                                                        cqlfilter += ")"
                                                }
                                                //cqlfilter += " " + datecol + " > '" + timefrom + "' and " + datecol + " < '" + timetill + "'"
                                        }
                                        break;
                                        // } else {

                                        // }
                                }

                        }
                }

                
                if (layerName == 'bin_size') {
                        sources = new ol.source.ImageWMS({
                                visible: true,
                                ratio: 1,
                                url: hostUrl + '/Notel/wms',
                                params: {
                                        'FORMAT': 'image/png',
                                        'VERSION': '1.1.0',
                                        "LAYERS": 'Notel:' + layerName,

                                        'CQL_FILTER': cqlfilter,
                                        "exceptions": 'application/vnd.ogc.se_inimage'
                                },
                                crossOrigin: 'anonymous',

                        });

                } else if (layerName == 'rfsig_tests_hexabins') {
                        var style
                        if (checkbox.parentElement.innerText == 'Tests Hexabins latency') {
                                cqlfilter += ' and measTestType = 2'
                                style = 'rfsig_tests_hexabins_latency'
                        } else if (checkbox.parentElement.innerText == 'Tests Hexabins download') {
                                cqlfilter += ' and measTestType = 0'
                                style = 'rfsig_tests_hexabins'
                        } else {
                                cqlfilter += ' and measTestType = 1'
                                style = 'rfsig_tests_hexabins'
                        }
                        sources = new ol.source.ImageWMS({
                                visible: true,
                                ratio: 1,
                                url: hostUrl + '/Notel/wms',
                                params: {
                                        'FORMAT': 'image/png',
                                        'VERSION': '1.1.0',
                                        "LAYERS": 'Notel:' + layerName,
                                        'CQL_FILTER': cqlfilter,
                                        'STYLES': style,
                                        "exceptions": 'application/vnd.ogc.se_inimage'
                                },
                                crossOrigin: 'anonymous',

                        });


                } else if (dropdownlayers.includes(layerName)) {
                        cqlfilter = 'created_time > ' + checkbox.parentElement.parentElement.parentElement.parentElement.getElementsByTagName('select')[0].value
                        sources = new ol.source.ImageWMS({
                                visible: true,
                                ratio: 1,
                                url: hostUrl + '/Notel/wms',
                                params: {
                                        'FORMAT': 'image/png',
                                        'VERSION': '1.1.0',
                                        "LAYERS": 'Notel:' + layerName,
                                        'CQL_FILTER': cqlfilter,
                                        'STYLES': stylename,
                                        "exceptions": 'application/vnd.ogc.se_inimage'
                                },
                                crossOrigin: 'anonymous',

                        });


                }
                // Parthesh Layer List
                else {  */
                if(adFilterapplied_arr.includes(layerName)){
                        if(allBinningLayers.find(o => o.layerName === layerName).exists){
                                for(i=0;i<noOfZoomLevels;i++){
                                        getLayerFromMap(layerName + '_bins_filter_' + i).setVisible(true);
                                }
                        }
                        getLayerFromMap(layerName + '_cql').setVisible(true);   
                }else{
                        // Remove layer if present
                        var lyr = getLayerFromMap(layerName);
                        if (lyr != null) {
                                myMap.removeLayer(lyr);
                                lyr = null;
                        }
                        var lyr = getLayerFromMap(layerName+'_cql');
                        if (lyr != null) {
                                myMap.removeLayer(lyr);
                                lyr = null;
                        }
                        sources = new ol.source.ImageWMS({
                                visible: true,
                                ratio: 1,
                                url: hostUrl + '/Notel/wms',
                                params: {
                                        'FORMAT': 'image/png',
                                        'VERSION': '1.1.0',
                                        "LAYERS": 'Notel:' + layerName,
                                        "singleTile": false,
                                        // 'STYLES': stylename,
                                        'CQL_FILTER': cqlfilter,
                                        "exceptions": 'application/vnd.ogc.se_inimage'
                                },
                                crossOrigin: 'anonymous',

                        });
                        // To check if it is a bin layer or point layer
                        if(allBinningLayers.find(o => o.layerName === layerName).exists){
                                binSource = new ol.source.ImageWMS({
                                        visible: true,
                                        ratio: 1,
                                        url: hostUrl + '/Notel/wms',
                                        params: {
                                                'FORMAT': 'image/png',
                                                'VERSION': '1.1.0',
                                                "LAYERS": 'Notel:' + layerName + '_bins',
                                                "singleTile": false,
                                                // 'STYLES': stylename,
                                                // 'CQL_FILTER': cqlfilter,
                                                "exceptions": 'application/vnd.ogc.se_inimage'
                                        },
                                        crossOrigin: 'anonymous',
                                });
                                // Remove bin layer if present
                                var lyr = getLayerFromMap(layerName + '_bins');
                                if (lyr != null) {
                                        myMap.removeLayer(lyr);
                                        lyr = null;
                                }
                                binLayer = new ol.layer.Image({
                                        source: binSource,
                                        maxResolution: 50000,
                                        minResolution: 1200 / 2 ** 4
                                });
                                binLayer.set('name', layerName + '_bins');
                                myMap.addLayer(binLayer);
                                // WMS layer added
                                untiled = new ol.layer.Image({
                                        source: sources,
                                        maxResolution: 1200 / 2 ** 4,
                                        minResolution: 0.00001
                                });
                                
                        }
                        else{
                                // If no binning layer is present
                                untiled = new ol.layer.Image({
                                        source: sources
                                }); 
                        }
                        untiled.set('name', layerName);
                        untiled.setOpacity(layeropacval)
                        myMap.addLayer(untiled);

                        
                }                
                // Remove CQL layer if present
                /*var lyr = getLayerFromMap(layerName + '_cql');
                if (lyr != null) {
                        myMap.removeLayer(lyr);
                        lyr = null;
                }*/
                infocomingfromlist = true
                layertolegend();
                document.getElementById('load').style.visibility = "hidden";

                // $('#infoaboutselectedlayer')[0].style.display = 'block'
                // $('#infoaboutselectedlayer').fadeOut(3000)
        }
        else {
                var index = turnedOnLayers.indexOf(layerName);
                if (index > -1) {
                        turnedOnLayers.splice(index, 1);
                }
                if(adFilterapplied_arr.includes(layerName)){
                  // Remove on the fly binning layer
                //   clearGeoBins(layerName);
                //   var index = adFilterapplied_arr.indexOf(layerName);
                //   if (index > -1) {
                //     adFilterapplied_arr.splice(index, 1);
                //   }
                        if(allBinningLayers.find(o => o.layerName === layerName).exists){
                                for(i=0;i<noOfZoomLevels;i++){
                                        getLayerFromMap(layerName + '_bins_filter_' + i).setVisible(false);
                                }   
                        }
                        getLayerFromMap(layerName + '_cql').setVisible(false);
                }else{
                        // Remove WMS layer
                        var untitledLayer = getLayerFromMap(layerName);
                        if (layerName == 'cell_modified_freq') {
                        }
                        if (untitledLayer != null) {
                                // untitledLayer.getSource().clear();
                                myMap.removeLayer(untitledLayer);
                                untitledLayer = null
                        }
                        // Remove binning layer
                        var binLayer = getLayerFromMap(layerName + '_bins');
                        if (binLayer != null) {
                                myMap.removeLayer(binLayer);
                                binLayer = null;
                        }
                        // Remove CQL filtered layer
                        var cqlLayer = getLayerFromMap(layerName + '_cql');
                        if (cqlLayer != null) {
                                // binLayer.getSource().clear();
                                myMap.removeLayer(cqlLayer);
                                cqlLayer = null;
                        }
                }
                // ol.Observable.unByKey(myMap.on('moveend', catchZoomLevelNoFilter));
                //remove layer from legend
                var alldiv = $('#createlegendoflayers .checkbox')
                for (i = 0; i < alldiv.length; i++) {
                        if (checkbox.parentElement.innerText == alldiv[i].getElementsByTagName('label')[0].innerText)
                                alldiv[i].parentNode.removeChild(alldiv[i])

                }

                checkbox.parentElement.parentElement.getElementsByClassName('taglabel')[0].style.backgroundColor = "rgba(0, 0, 0, 0.5)"

                if (identifypopupLayerName == layerName) {
                        //close identifypopup
                        $('#popup-closer').click()
                        //
                }

        }

        $("body").css("cursor", "default");
}



function getLayerFromMap(layername) {

        var layer = null;
        var layersArray = myMap.getLayers().getArray();
        for (var i = 0; i < layersArray.length; i++) {
                var lyr = layersArray[i];
                if (lyr instanceof ol.layer.Image || lyr instanceof ol.layer.Vector) {

                        if (lyr.getProperties().name == layername) {
                                layer = lyr;
                                break;
                        }

                }

        }
        return layer;
}


//move legend one step up
function takelegendup(ele) {
        eleforzindex = ele
        var currentlabel = ele.parentNode.parentElement.getElementsByTagName('label')[0].innerText
        var alldiv = $('#createlegendoflayers .checkbox')
        for (i = 1; i < alldiv.length; i++) {
                if (currentlabel == alldiv[i].getElementsByTagName('label')[0].innerText) {
                        var div1 = alldiv[i]
                        var div2 = alldiv[i - 1]
                        var parent = div1.parentNode;

                        parent.insertBefore(div1, div2);
                } else {

                }
        }
        setZIndex()
}

//move legend one step down
function takelegenddown(ele) {
        eleforzindex = ele
        var currentlabel = ele.parentNode.parentElement.getElementsByTagName('label')[0].innerText
        var alldiv = $('#createlegendoflayers .checkbox')
        for (i = 0; i < alldiv.length; i++) {
                if (currentlabel == alldiv[i].getElementsByTagName('label')[0].innerText) {
                        if (i < (alldiv.length - 1)) {
                                var div1 = alldiv[i]
                                var div2 = alldiv[i + 1]
                                var parent = div1.parentNode;

                                parent.insertBefore(div2, div1);
                                break
                        } else {

                        }

                }
        }
        setZIndex()
}

//zoom to specific layer 
function zoomtolayer(ele) {
        document.getElementById('load').style.visibility = "visible";
        var layerName = ele.parentNode.parentElement.id.split(":")[1]
        var tempinfocomingfromlist = false
        for (u = 0; u < alllayerdataforrc.length; u++) {
                if (layerName == alllayerdataforrc[u].layername) {
                        tempinfocomingfromlist = true
                }
        }

        if (tempinfocomingfromlist == false) {
                if (layerName == 'geotiff') {
                        zoomtogeotiff()
                } else {
                        myMap.getLayers().forEach(
                                function (layer) {
                                        if (layer != undefined) {
                                                var name = layer.get('name');
                                                if (name === layerName) {
                                                        // myMap.removeLayer(layer);
                                                        var ext = layer.getSource().getExtent()
                                                        myMap.getView().fit(ext)
                                                        document.getElementById('load').style.visibility = "hidden";

                                                        if (name === "highlightlayer") {
                                                                myMap.getView().setZoom(16);
                                                        }
                                                        // document.getElementById("tab_content").innerHTML = "";
                                                        // document.getElementById("dataGrid_tab").innerHTML = "";
                                                }
                                        }

                                });
                }

        }
        else {
                if (alllayerinfofromgetcapabilites) {
                        var Layers = alllayerinfofromgetcapabilites
                        var extent;
                        for (var i = 0; i < Layers.length; i++) {
                                var layerobj = Layers[i];
                                if (layerobj.Name == "Notel:" + layerName) {

                                        extent = layerobj.BoundingBox[0].extent;
                                        // var ex = ol.proj.transformExtent(extent, 'EPSG:4326', 'EPSG:3857');
                                }

                        }
                        myMap.getView().fit(extent, myMap.getSize());
                        document.getElementById('load').style.visibility = "hidden";

                        // myMap.getView().setResolution(600);
                } else {
                        var url = hostUrl + '/wms?request=GetCapabilities&service=WMS&version=1.1.0';
                        var parser = new ol.format.WMSCapabilities();
                        $.ajax(url).then(function (response) {
                                var result = parser.read(response);

                                var Layers = result.Capability.Layer.Layer;
                                alllayerinfofromgetcapabilites = Layers
                                var extent;
                                for (var i = 0; i < Layers.length; i++) {
                                        var layerobj = Layers[i];
                                        if (layerobj.Name == "Notel:" + layerName) {

                                                extent = layerobj.BoundingBox[0].extent;
                                                // var ex = ol.proj.transformExtent(extent, 'EPSG:4326', 'EPSG:3857');
                                        }

                                }
                                myMap.getView().fit(extent, myMap.getSize());
                                // myMap.getView().setResolution(600);
                                document.getElementById('load').style.visibility = "hidden";

                        });
                }
        }
}

function setZIndex() {

        //var layerName = eleforzindex.parentNode.parentElement.id.split(":")[1]
        var alllayersonmap = myMap.getLayers().array_
        var allactivatedlayers = eleforzindex.parentNode.parentElement.parentElement.getElementsByTagName('div')

        for (i = allactivatedlayers.length; i > 0; i--) {
                var layerName = allactivatedlayers[allactivatedlayers.length - i].id.split(":")[1]
                for (j = 0; j < alllayersonmap.length; j++) {
                        if (layerName == alllayersonmap[j].values_.name) {
                                alllayersonmap[j].setZIndex(i)
                        }
                }

        }

}


//toggle advance options in layerlist
function toggleextraop() {
        if (document.getElementById('advanceopinlayerlist').style.display == 'none') {
                document.getElementById('advanceopinlayerlist').style.display = 'block';
                // document.getElementById('layersublistdivFilter').style.display = 'block';
        } else {
                document.getElementById('advanceopinlayerlist').style.display = 'none';
                // document.getElementById('layersublistdivFilter').style.display = 'none'
                // $('#timefrom')[0].value = ''
                // $('#timetill')[0].value = ''
                $('#operatorselect').val([])
                $('#technologyselect').val([])
        }
}


// reset all advance options 
function resetadvanceop() {
        // advancedFilterApplied = false;
        resettimefrom()
        resettimetill()
        $('#operatorselect').val([])
        $('#technologyselect').val([])
        resetalllayers()

}

//reset time from
function resettimefrom() {
        $('#timefrom')[0].value = ''
}
//reset time till
function resettimetill() {
        $('#timetill')[0].value = ''
}
//reset operators
function resetopinadvanceop() {
        $('#operatorselect').val([])
}


//reset technology
function resettechinadvanceop() {
        $('#technologyselect').val([])
}





searchBox = document.querySelector("#advsearchop");
soperators = document.querySelector("#operatorselect");
var when = "keyup";

searchBox.addEventListener("keyup", function (e) {
        var text = e.target.value;
        var options = soperators.options;

        var indexVal;
        var newarr = [];

        if (text != "") {
                for (var i = 0; i < options.length; i++) {
                        var option = options[i];
                        var optionText = option.text;
                        var lowerOptionText = optionText.toLowerCase();
                        var lowerText = text.toLowerCase();
                        var regex = new RegExp("^" + text, "i");
                        var match = optionText.match(regex);
                        var contains = lowerOptionText.indexOf(lowerText) != -1;
                        if (match || contains) {
                                //     option.selected = true;
                                //     return;
                                newarr.push(option);

                        }


                }
        }
        else {
                newarr = [];
        }



        if (newarr.length == 0) {
                //searchBox.selectedIndex = 0;
                newarr = allOpList;
        }

        $('#operatorselect').empty()
        var selectdrop = $('#operatorselect')[0]
        for (var z = 0; z < newarr.length; z++) {
                selectdrop.appendChild(newarr[z])
        }


        var sortSelect = function (select, attr, order) {
                if (attr === 'text') {
                        if (order === 'asc') {
                                $(select).html($(select).children('option').sort(function (x, y) {
                                        return $(x).text().toUpperCase() < $(y).text().toUpperCase() ? -1 : 1;
                                }));
                                $(select).get(0).selectedIndex = 0;

                        }// end asc
                        if (order === 'desc') {
                                $(select).html($(select).children('option').sort(function (y, x) {
                                        return $(x).text().toUpperCase() < $(y).text().toUpperCase() ? -1 : 1;
                                }));
                                $(select).get(0).selectedIndex = 0;

                        }// end desc
                }

        };

        sortSelect('#operatorselect', 'text', 'asc');

        //     $('#operatorselect').val([]);
        //     soperators.appendChild(newarr);

});


function addClassForOpeningDiv(d) {

        if (document.getElementById("internaldiv-" + d.id).className.includes("showlaydiv")) {

                d.className = "la la-angle-double-right"
                document.getElementById("internaldiv-" + d.id).className = "internaldiv"

        }
        else {

                d.className += " showarrowdown"
                document.getElementById("internaldiv-" + d.id).className += " showlaydiv"
        }


}

var flagOfNextGeobin = true;
// var advancedFilterApplied = false;
var adFilterapplied_arr = [];
function applyadvanceop() {
        // advancedFilterApplied = true;
        var fromDate = $("#timefrom").val();
        var toDate = $("#timetill").val();
        var technology = $("#technologyselect").val();
        var operator = $("#operatorselect").val();
        console.log(fromDate, toDate, technology, operator);
        // Get all checked layers and disable them
        if ((fromDate && toDate) || (technology.length > 0 || operator.length > 0)) {
                // var inputBox = document.getElementById("layersublistdiv").querySelectorAll("input");
                // for (i = 0; i < inputBox.length; i++) {
                        // if (inputBox[i].checked) {
                for(i=0;i<turnedOnLayers.length;i++){
                        
                                // checkedName = inputBox[i].name;
                                let checkedName = turnedOnLayers[i];
                                adFilterapplied_arr.push(turnedOnLayers[i]);  // Filter applied on layername
                                // console.log("checkedName",checkedName,i);
                                // To check if bin layer exists or point layer
                                //if(allBinningLayers.find(o => o.layerName === checkedName).exists){
                                for(j=0;j<allBinningLayers.length;j++){
                                        if(allBinningLayers[j].layerName==turnedOnLayers[i] && allBinningLayers[j].exists){
                                                console.log('Processing GeoHexaBins for ',turnedOnLayers[i]);
                                                getGeoBins(checkedName, fromDate, toDate, technology, operator);
                                        }
                                }

                        }

                
        
                // });

                var layerdatalayernamearray = []
                for (f = 0; f < layerdata.length; f++) {
                        layerdatalayernamearray.push(layerdata[f].layername)
                }
                document.getElementById('load').style.visibility = "visible";
                var allonlayers = []
                myMap.getLayers().forEach(
                        function (layer) {
                                if (layer != undefined) {
                                        var name = layer.get('name');
                                        if (name != undefined) {
                                                allonlayers.push(layer)

                                        }
                                }

                        });
                for (n = 0; n < allonlayers.length; n++) {
                        if (layerdatalayernamearray.includes(allonlayers[n].values_.name)) {
                                var stylename = allonlayers[n].getSource().params_.STYLES
                                layerName = allonlayers[n].values_.name
                                // console.log(layerName);
                                // myMap.removeLayer(allonlayers[n]);
                                for (i = 0; i < layerdata.length; i++) {
                                        if (layerName == layerdata[i].layername) {
                                                var cqlfilter = '1=1'
                                                var datecol = layerdata[i].datecol
                                                if (datecol == "") {
                                                        datecol = null
                                                }
                                                var timefrom = $('#timefrom')[0].value
                                                var timetill = $('#timetill')[0].value
                                                var operatorcol = layerdata[i].operatorcol
                                                if (operatorcol == "") {
                                                        operatorcol = null
                                                }
                                                if ($('#selectallforop')[0].checked) {
                                                        var allop = ''
                                                } else {
                                                        var allop = $('#operatorselect').val()
                                                }

                                                var techcol = layerdata[i].techcol
                                                if (techcol == "") {
                                                        techcol = null
                                                }
                                                if ($('#selectallfortech')[0].checked) {
                                                        var alltech = ''
                                                } else {
                                                        var alltech = $('#technologyselect').val()
                                                }


                                                if (datecol != null) {
                                                        if (timefrom == "" && timetill == '') {

                                                        } else {
                                                                if (timefrom == "") {
                                                                        cqlfilter += " and " + datecol + " <= '" + timetill + "'"

                                                                } else if (timetill == '') {
                                                                        cqlfilter += " and " + datecol + " >= '" + timefrom + "'"
                                                                } else if (timetill == timefrom) {
                                                                        var datetimetill = new Date(timetill)
                                                                        datetimetill.setDate(datetimetill.getDate() + 1)

                                                                        var temptimetill = datetimetill.toISOString().substr(0, 10);
                                                                        cqlfilter += " and " + datecol + " >= '" + timefrom + "' and " + datecol + " <= '" + temptimetill + "'"
                                                                }
                                                                else {
                                                                        cqlfilter += " and " + datecol + " >= '" + timefrom + "' and " + datecol + " <= '" + timetill + "'"
                                                                }
                                                        }
                                                }
                                                if (operatorcol != null) {
                                                        if (allop[0] != "None" && allop[0] != "" && allop[0] != undefined) {
                                                                cqlfilter += " and " + operatorcol + " in ("
                                                                for (i = 0; i < allop.length; i++) {
                                                                        if (i < 1) {
                                                                                cqlfilter += parseInt(allop[i])
                                                                        } else {
                                                                                cqlfilter += "," + parseInt(allop[i])
                                                                        }
                                                                }
                                                                cqlfilter += ")"
                                                        }
                                                        //cqlfilter += " " + datecol + " > '" + timefrom + "' and " + datecol + " < '" + timetill + "'"
                                                }
                                                if (techcol != null) {
                                                        if (alltech[0] != "None" && alltech[0] != "" && alltech[0] != undefined) {
                                                                cqlfilter += " and " + techcol + " in ("
                                                                for (i = 0; i < alltech.length; i++) {
                                                                        if (i < 1) {
                                                                                cqlfilter += "'" + alltech[i] + "'"
                                                                        } else {
                                                                                cqlfilter += ",'" + alltech[i] + "'"
                                                                        }
                                                                }
                                                                cqlfilter += ")"
                                                        }
                                                        //cqlfilter += " " + datecol + " > '" + timefrom + "' and " + datecol + " < '" + timetill + "'"
                                                }
                                                // showLayerOnMap(allcheckboxes[z])

                                                /*if (layerName == 'bin_size') {
                                                        sources = new ol.source.ImageWMS({
                                                                visible: true,
                                                                ratio: 1,
                                                                url: hostUrl + '/Notel/wms',
                                                                params: {
                                                                        'FORMAT': 'image/png',
                                                                        'VERSION': '1.1.0',
                                                                        "LAYERS": 'Notel:' + layerName,

                                                                        'CQL_FILTER': cqlfilter,
                                                                        "exceptions": 'application/vnd.ogc.se_inimage'
                                                                },
                                                                crossOrigin: 'anonymous',

                                                        });

                                                } else if (layerName == 'rfsig_tests_hexabins') {
                                                        var style
                                                        if (allonlayers[n].getSource().params_.CQL_FILTER.slice(-1) == '2') {
                                                                cqlfilter += ' and measTestType = 2'
                                                                style = 'rfsig_tests_hexabins_latency'
                                                        } else if (allonlayers[n].getSource().params_.CQL_FILTER.slice(-1) == '0') {
                                                                cqlfilter += ' and measTestType = 0'
                                                                style = 'rfsig_tests_hexabins'
                                                        } else {
                                                                cqlfilter += ' and measTestType = 1'
                                                                style = 'rfsig_tests_hexabins'
                                                        }
                                                        sources = new ol.source.ImageWMS({
                                                                visible: true,
                                                                ratio: 1,
                                                                url: hostUrl + '/Notel/wms',
                                                                params: {
                                                                        'FORMAT': 'image/png',
                                                                        'VERSION': '1.1.0',
                                                                        "LAYERS": 'Notel:' + layerName,
                                                                        'CQL_FILTER': cqlfilter,
                                                                        // 'STYLES': style,
                                                                        "exceptions": 'application/vnd.ogc.se_inimage'
                                                                },
                                                                crossOrigin: 'anonymous',

                                                        });


                                                }
                                                else if (dropdownlayers.includes(layerName)) {
                                                        cqlfilter = '1=1'
                                                        if (operatorcol != null) {
                                                                if (allop[0] != "None" && allop[0] != "" && allop[0] != undefined) {
                                                                        cqlfilter += " and " + operatorcol + " in ("
                                                                        for (i = 0; i < allop.length; i++) {
                                                                                if (i < 1) {
                                                                                        cqlfilter += parseInt(allop[i])
                                                                                } else {
                                                                                        cqlfilter += "," + parseInt(allop[i])
                                                                                }
                                                                        }
                                                                        cqlfilter += ")"
                                                                }
                                                                //cqlfilter += " " + datecol + " > '" + timefrom + "' and " + datecol + " < '" + timetill + "'"
                                                        }
                                                        if (techcol != null) {
                                                                if (alltech[0] != "None" && alltech[0] != "" && alltech[0] != undefined) {
                                                                        cqlfilter += " and " + techcol + " in ("
                                                                        for (i = 0; i < alltech.length; i++) {
                                                                                if (i < 1) {
                                                                                        cqlfilter += "'" + alltech[i] + "'"
                                                                                } else {
                                                                                        cqlfilter += ",'" + alltech[i] + "'"
                                                                                }
                                                                        }
                                                                        cqlfilter += ")"
                                                                }
                                                                //cqlfilter += " " + datecol + " > '" + timefrom + "' and " + datecol + " < '" + timetill + "'"
                                                        }
                                                        cqlfilter += 'and created_time > ' + document.getElementById('selectof' + layerName + '').value
                                                        sources = new ol.source.ImageWMS({
                                                                visible: true,
                                                                ratio: 1,
                                                                url: hostUrl + '/Notel/wms',
                                                                params: {
                                                                        'FORMAT': 'image/png',
                                                                        'VERSION': '1.1.0',
                                                                        "LAYERS": 'Notel:' + layerName,
                                                                        'CQL_FILTER': cqlfilter,
                                                                        // 'STYLES': stylename,
                                                                        "exceptions": 'application/vnd.ogc.se_inimage'
                                                                },
                                                                crossOrigin: 'anonymous',

                                                        });


                                                }*/
                                                // else {
                                                var lyr = getLayerFromMap(layerName);
                                                if(lyr!=null){myMap.removeLayer(lyr);lyr=null;}
                                                console.log(cqlfilter);
                                                sources = new ol.source.ImageWMS({
                                                        visible: true,
                                                        ratio: 1,
                                                        url: hostUrl + '/Notel/wms',
                                                        params: {
                                                                'FORMAT': 'image/png',
                                                                'VERSION': '1.1.0',
                                                                "LAYERS": 'Notel:' + layerName,
                                                                "singleTile": false,
                                                                // 'STYLES': stylename,
                                                                'CQL_FILTER': cqlfilter,
                                                                "exceptions": 'application/vnd.ogc.se_inimage'
                                                        },
                                                        crossOrigin: 'anonymous',

                                                });
                                                // }
                                                
                                                untiled = new ol.layer.Image({
                                                        source: sources,
                                                        minResolution: 0.00001,
                                                        maxResolution: adFilterapplied_arr.includes(layerName)? 1200 / 8:500000000
                                                });
                                                untiled.set('name', layerName + '_cql');
                                                console.log(layerName + "_cql added")
                                                untiled.setOpacity(layeropacval)
                                                myMap.addLayer(untiled);
                                                // untiled.setVisible(false); // Should only be visible if zoom more than bin layer
                                                break
                                        }
                                }
                        }
                }

                document.getElementById('load').style.visibility = "hidden";

                //close identifypopup
                $('#popup-closer').click()
                //
        } else {
                alert("Please chose appropriate filters !");
                return;
        }
}


//reset all layers
function resetalllayers() {
        for(i=0;i<adFilterapplied_arr.length;i++){
                clearGeoBins(adFilterapplied_arr[i]);
                var lyr = getLayerFromMap(adFilterapplied_arr[i]+'_cql');       //remove cql layer
                if(lyr!=null){
                        myMap.removeLayer(lyr);
                        lyr = null;
                }
        }
        adFilterapplied_arr = [];
        // $("#layersublistdiv").each(function () {
        var inputBox = document.getElementById("layersublistdiv").querySelectorAll("input");
        for(i=0;i<inputBox.length;i++){
                if (inputBox[i].checked) {
                        // var index = adFilterapplied_arr.indexOf(inputBox[0].name);
                        // if (index > -1) {
                        //   adFilterapplied_arr.splice(index, 1);
                        // }
                        showLayerOnMap(inputBox[i]);
                }
        }
        // });
}
var allcolorsoflayersforbin = {
        "name": "active_test_upload",
        "ranges": [
                [
                        0,
                        5
                ],
                [
                        5,
                        15
                ],
                [
                        15,
                        30
                ],
                [
                        30,
                        50
                ],
                [
                        50,
                        100
                ],
                [
                        100,
                        300
                ]
        ],
        "colorschema": [
                "#e31a1c",
                "#ff7f00",
                "#fdbf00",
                "#33a02c",
                "#32cd32",
                "#1f78b4"
        ]
}
// $.getJSON('./layerconfig.json', function (data) {
//         allcolorsoflayersforbin = data
// })



function getGeoBins(layerChosen, fromDate, toDate, technology, operator) {
        // var obj = $.grep(layerdata, function(obj){return obj.layername === layerName;})[0];
        // var fromDate = $("#timefrom").val();
        // var toDate = $("#timetill").val();
        // var technology = $("#technologyselect").val();
        // var operator = $("#operatorselect").val();
        // layerChosen = 'active_test_upload';
        clearGeoBins(layerChosen);
        queryField = 'count';
        // console.log(layerChosen, queryField, fromDate, toDate);
        // if(fromDate && toDate ){
        myMap.getTargetElement().style.cursor = 'progress';
        // flagOfNextGeobin = false
        $.post('/getGeoBins/', { 'layer': layerChosen, 'operation': queryField, 'operator': operator, 'technology': technology, 'fromDate': fromDate, 'toDate': toDate }, function (data) {
                if (data.length) {
                        // flagOfNextGeobin = true

                        for (j = 0; j < data.length; j++) {
                                geoBin_zoom = new ol.format.GeoJSON({
                                        defaultDataProjection: 'EPSG:4326',
                                        featureProjection: 'EPSG:3857'
                                });
                                var vectorSource = new ol.source.Vector({
                                        features: geoBin_zoom.readFeatures(data[j])
                                });
                                var vectorLayer = new ol.layer.Vector({
                                        source: vectorSource,
                                        opacity: 0.75,
                                        maxResolution: j == 0 ? 99999999 : (1200 / 2 ** j),
                                        minResolution: 1200 / 2 ** (j + 1),
                                        style: function (feature, resolution) {

                                                for (p = 0; p < allcolorRangesForBinning.length; p++) {
                                                        if (allcolorRangesForBinning[p].name == layerChosen) {
                                                                if (typeof (allcolorRangesForBinning[p].colorschema) == 'object') {
                                                                        var allcolors = allcolorRangesForBinning[p].colorschema
                                                                        for (u = 0; u < allcolors.length; u++) {
                                                                                window["colors" + u] = new ol.style.Style({
                                                                                        fill: new ol.style.Fill({
                                                                                                color: allcolors[u]
                                                                                        }),
                                                                                        stroke: new ol.style.Stroke({
                                                                                                color: '#000000',
                                                                                                width: 2
                                                                                        }),

                                                                                });
                                                                        }
                                                                        var allranges = allcolorRangesForBinning[p].ranges
                                                                        if (allranges.length == allcolors.length) {
                                                                                for (j = 0; j < allranges.length; j++) {
                                                                                        if (allcolorRangesForBinning[p].denominator && allcolorRangesForBinning[p].denominator != 0) {
                                                                                                if (allranges[j][0] < feature.getProperties().value / allcolorRangesForBinning[p].denominator < allranges[j][1]) {
                                                                                                        return [window['colors' + j]]
                                                                                                        break
                                                                                                }
                                                                                        } else {
                                                                                                if (allranges[j][0] < feature.getProperties().value < allranges[j][1]) {
                                                                                                        return [window['colors' + j]]
                                                                                                        break
                                                                                                }
                                                                                        }

                                                                                }
                                                                        } else {
                                                                                Swal.fire({
                                                                                        title: 'Number of ranges and colors does not match! ',
                                                                                        type: 'error',
                                                                                        timer: 2000,
                                                                                        position: 'top-end',
                                                                                        showConfirmButton: false

                                                                                })
                                                                        }
                                                                } else if (typeof (allcolorRangesForBinning[p].colorschema) == 'string') {
                                                                        var color = new ol.style.Style({
                                                                                fill: new ol.style.Fill({
                                                                                        color: allcolorRangesForBinning[p].colorschema
                                                                                }),
                                                                                stroke: new ol.style.Stroke({
                                                                                        color: '#000000',
                                                                                        width: 2
                                                                                }),

                                                                        });
                                                                        return [color]
                                                                } else {
                                                                        Swal.fire({
                                                                                title: 'error in JSON file for given layer! ',
                                                                                type: 'error',
                                                                                timer: 2000,
                                                                                position: 'top-end',
                                                                                showConfirmButton: false

                                                                        })
                                                                }
                                                        }
                                                }

                                        }
                                        // style:hexBinStyleFunc
                                });
                                vectorLayer.set('name', layerChosen + '_bins_filter_' + j);
                                myMap.addLayer(vectorLayer);
                        }
                        var lyr = getLayerFromMap(layerChosen+'_bins');
                        console.log("removing ",layerChosen+'_bins');
                        if(lyr!=null){
                                myMap.removeLayer(lyr);
                                lyr = null;
                        }
                        var lyr = getLayerFromMap(layerChosen);
                        if(lyr!=null){
                                myMap.removeLayer(lyr);
                                lyr = null;
                        }
                        myMap.getTargetElement().style.cursor = '';
                        // removeDefaultLayer(layerName);
                }else { alert("Sorry, No data available!"); myMap.getTargetElement().style.cursor = ''; }
                
        });
        // return;
}

var hexBinStyleFunc = function (f, res) {
        var color;
        if (f.get('features').length > max) color = [136, 0, 0, .8];
        else if (f.get('features').length > min) color = [255, 165, 0, .8];
        else color = [0, 136, 0, .8];
        return [new ol.style.Style({ fill: new ol.style.Fill({ color: color }) })];
}

function clearGeoBins(layerChosen) {
        for (var i = 0; i < noOfZoomLevels; i++) {
                var lyr = getLayerFromMap(layerChosen + '_bins_filter_' + i);
                if (lyr != null) {  lyr.getSource().clear(); myMap.removeLayer(lyr); lyr = null; }
        }
}

function removeDefaultLayer(layerName) {
        var untitledLayer = getLayerFromMap(layerName);
        if (layerName == 'cell_modified_freq') {
        }
        if (untitledLayer != null) {
                myMap.removeLayer(untitledLayer);
                untitledLayer = null;
        }
        var binLayer = getLayerFromMap(layerName + '_bins');
        if (binLayer != null) {
                myMap.removeLayer(binLayer);
                binLayer = null;
        }
}


