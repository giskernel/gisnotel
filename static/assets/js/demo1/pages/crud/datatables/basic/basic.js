"use strict";
var UserManagement = function () {

	var initTable = function () {
		var modal = $('#manageUserModal');
		var table = $('#kt_table_1');

		var datatable = table.KTDatatable({
			responsive: false,
			layout: {
				scroll: true,
				footer: false,
			},
			sortable: true,
			pagination: true,
			search: {
				input: $('#generalSearch'),
			},
			columns: [
				{
					field: 'User Name',
					title: 'User Name',
					autoHide: false,
					overflow: 'visible',
					width: 80
				},
				{
					field: 'Email',
					title: 'Email',
					autoHide: false,
					overflow: 'visible',
					width: 80
				},
				{
					field: 'Status',
					title: 'Status',
					autoHide: false,
					overflow: 'visible'
				},
				{
					field: 'Country',
					title: 'Country',
					autoHide: false,
					overflow: 'visible'
				},
				{
					field: 'Operator',
					title: 'Operator',
					autoHide: false,
					overflow: 'visible'
				},
				{
					field: 'Time From',
					title: 'Time From',
					autoHide: false,
					overflow: 'visible'
				},
				{
					field: 'Time till',
					title: 'Time till',
					autoHide: false,
					overflow: 'visible'
				},
				{
					field: 'Layer Privilages',
					title: 'Layer Privilages',
					autoHide: false,
					overflow: 'visible'
				},
				{
					field: 'Action',
					title: 'Action',
					autoHide: false,
					sortable: false,
					overflow: 'visible',
					width: 60
				}
			],
		});
		datatable.hide();
		modal.on('shown.bs.modal', function () {
			var modalContent = $(this).find('.modal-content');
			datatable.spinnerCallback(true, modalContent);
			datatable.reload();

			datatable.on('kt-datatable--on-layout-updated', function () {
				datatable.show();
				datatable.spinnerCallback(false, modalContent);
				datatable.redraw();


				$('.kt_datepicker_minimum_timefrom').datepicker({
					format: 'yyyy-mm-dd',
					todayHighlight: true,

				}).on('changeDate', function (selected) {
					var minDate = new Date(selected.date.valueOf());
					$('.kt_datepicker_minimum_timetill').datepicker('setStartDate', minDate);
				});

				$('.kt_datepicker_minimum_timetill').datepicker({
					format: 'yyyy-mm-dd',
					todayHighlight: true,
				}).on('changeDate', function (selected) {
					var minDate = new Date(selected.date.valueOf());
					$('.kt_datepicker_minimum_timefrom').datepicker('setEndDate', minDate);
				});

				$('.kt-select2-country, .kt-select2-operator, .kt-select2-layers').select2({
					placeholder: "Everything Selected"
				});
				populatetable();
			});
		});

	};

	// var initTable1 = function () {
	// 	var table = $("#userslist");

	// 	// begin first table
	// 	table.DataTable({
	// 		responsive: true,

	// 		// DOM Layout settings
	// 		dom: `<'row'<'col-sm-12'tr>>
	// 			<'row'<'col-sm-12 col-md-5'i><'col-sm-12 col-md-7 dataTables_pager'lp>>`,

	// 		lengthMenu: [5, 10, 25, 50],

	// 		pageLength: 10,

	// 		language: {
	// 			lengthMenu: "Display _MENU_"
	// 		},

	// 		// Order settings
	// 		order: [[1, "desc"]],

	// 		headerCallback: function (thead, data, start, end, display) {
	// 			thead.getElementsByTagName("th")[0].innerHTML = `
	// 					<label class="kt-checkbox kt-checkbox--single kt-checkbox--solid">
	// 						<input type="checkbox" id='selectallusers' value="" class="m-group-checkable">
	// 						<span></span>
	// 					</label>`;
	// 		},

	// 		columnDefs: [
	// 			{
	// 				targets: 0,
	// 				width: "30px",
	// 				className: "dt-right",
	// 				orderable: false,
	// 				render: function (data, type, full, meta) {
	// 					return `
	// 				<label class="kt-checkbox kt-checkbox--single kt-checkbox--solid">
	// 					<input type="checkbox" value="" class="m-checkable">
	// 					<span></span>
	// 				</label>`;
	// 				}
	// 			}

	// 		]
	// 	});

	// 	table.on("change", ".kt-group-checkable", function () {
	// 		var set = $(this)
	// 			.closest("table")
	// 			.find("td:first-child .kt-checkable");
	// 		var checked = $(this).is(":checked");

	// 		$(set).each(function () {
	// 			if (checked) {
	// 				$(this).prop("checked", true);
	// 				$(this)
	// 					.closest("tr")
	// 					.addClass("active");
	// 			} else {
	// 				$(this).prop("checked", false);
	// 				$(this)
	// 					.closest("tr")
	// 					.removeClass("active");
	// 			}
	// 		});
	// 	});

	// 	table.on("change", "tbody tr .kt-checkbox", function () {
	// 		$(this)
	// 			.parents("tr")
	// 			.toggleClass("active");
	// 	});
	// };
	return {
		init: function () {
			initTable();
			//initTable1();
		}
	};
}();

jQuery(document).ready(function () {
	UserManagement.init();
});


var KTDatatablesBasicBasic = function () {

	var initTable1 = function () {
		var table = $('#datagridtable');

		// begin first table
		table.DataTable({

			scrollY: '50vh',
			scrollX: true,
			scrollCollapse: true,
			// DOM Layout settings


			// Order settings

			// headerCallback: function (thead, data, start, end, display) {
			// 	thead.getElementsByTagName('th')[0].innerHTML = `
			//         <label class="kt-checkbox kt-checkbox--single kt-checkbox--solid">
			//             <input type="checkbox" value="" class="m-group-checkable">
			//             <span></span>
			//         </label>`;
			// },

			// columnDefs: [

			// 	{
			// 		targets: 0,
			// 		title: 'Actions',
			// 		orderable: false,
			// 		render: function (data, type, full, meta) {
			// 			return `
			//             <span class="dropdown">
			//                 <a href="#" class="btn btn-sm btn-clean btn-icon btn-icon-md" data-toggle="dropdown" aria-expanded="true">
			//                   <i class="la la-ellipsis-h"></i>
			//                 </a>
			//                 <div class="dropdown-menu dropdown-menu-right">
			//                     <a class="dropdown-item" href="#"><i class="la la-edit"></i> Edit Details</a>
			//                     <a class="dropdown-item" href="#"><i class="la la-leaf"></i> Update Status</a>
			//                     <a class="dropdown-item" href="#"><i class="la la-print"></i> Generate Report</a>
			//                 </div>
			//             </span>
			//             <a href="#" class="btn btn-sm btn-clean btn-icon btn-icon-md" title="View">
			//               <i class="la la-edit"></i>
			//             </a>`;
			// 		},
			// 	}
			// ],
		});

		table.on('change', '.kt-group-checkable', function () {
			var set = $(this).closest('table').find('td:first-child .kt-checkable');
			var checked = $(this).is(':checked');

			$(set).each(function () {
				if (checked) {
					$(this).prop('checked', true);
					$(this).closest('tr').addClass('active');
				}
				else {
					$(this).prop('checked', false);
					$(this).closest('tr').removeClass('active');
				}
			});
		});

		table.on('change', 'tbody tr .kt-checkbox', function () {
			$(this).parents('tr').toggleClass('active');
		});
	};

	return {

		//main function to initiate the module
		init: function () {
			initTable1();
		},

	};

}();

jQuery(document).ready(function () {
	KTDatatablesBasicBasic.init();
});