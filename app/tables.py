import django_tables2 as tables
from .models import User


class userTable(tables.Table):
    class Meta:
        model = User
        template_name = 'gis/map.html'
