from app.models import LayerListData
from django.http import JsonResponse
from django.db import connection,connections,transaction
from django.core import serializers
from app.models import UserProfileInfo as userprofile
from django.forms import inlineformset_factory
from django.contrib.auth.forms import UserChangeForm
from django.views.generic import UpdateView
from app.models import User as userbase
from django.shortcuts import render, redirect, get_object_or_404
from app.forms import UserForm, UserProfileInfoForm, userupdateform, userprofileinfoupdteform
# from django.urls import reverse
from django.contrib.auth.decorators import login_required
from django.http import HttpResponse, HttpResponseRedirect
from django.contrib.auth import authenticate, login, logout
import math,os,json
import datetime
import numpy as np
from h3 import h3
import pandas as pd
import geopandas as gp
from shapely.geometry import Polygon
from sqlalchemy import *
from django.views.decorators.csrf import csrf_exempt


# Create your views here.


def index(request):
    if request.method == 'POST':
        username = request.POST.get('username')
        password = request.POST.get('password')

        user = authenticate(username=username, password=password)


def user_login(request):
    if request.user.is_authenticated:
        return map(request)
    else:
        if request.method == 'POST':
            # email = request.POST['email']
            userinput = request.POST['username']

            try:
                username = userbase.objects.get(email=userinput).username
            except userbase.DoesNotExist:
                username = request.POST['username']
            password = request.POST['password']

            user = authenticate(username=username, password=password)
            if user is not None:
                if user.is_active:
                    login(request, user)
                    # Redirect to index page.
                    return HttpResponseRedirect("map/")
                else:
                    not_act = {
                        'not_active': 'Account is not active yet, Please contact Administrator at giskernel@admin.com'}
                    return render(request, 'nongis/index.html', not_act)
            else:
                # Return an 'invalid login' error message.
                error = {
                    'wrong': '  Please Enter Valid Credentials'}
                return render(request, 'nongis/index.html', error)
        else:
            # the login is a  GET request, so just show the user the login form.
            return render(request, 'nongis/index.html')


def register(request):
    registered = False
    if request.user.is_authenticated:
        return map(request)
    else:
        if request.method == 'POST':
            user_form = UserForm(request.POST)
            profile_form = UserProfileInfoForm(request.POST)
            if user_form.is_valid() and profile_form.is_valid():
                human = True
                user = user_form.save()
                user.set_password(user.password)
                user.is_active = False
                user.save()

                profile = profile_form.save(commit=False)
                profile.user = user

                if 'profile_pic' in request.FILES:
                    profile.profile_pic = request.FILES['profile_pic']

                profile.save()

                registered = True

            else:
                print(user_form.errors, profile_form.errors)

        else:
            user_form = UserForm()
            profile_form = UserProfileInfoForm()

        return render(request, 'nongis/register.html', {'user_form': user_form, 'profile_form': profile_form, 'registered': registered})


@login_required
def map(request):
    usermain = request.user
    id = usermain.id
    profileinfo = userprofile.objects.get(user_id=id)
    all_user = userprofile.objects.all()
    if request.method == 'POST':
        u_form = userupdateform(request.POST, instance=request.user)
        p_form = userprofileinfoupdteform(request.POST, request.FILES, instance=profileinfo)

        if u_form.is_valid() and p_form.is_valid():
           u_form.save()
           p_form.save()
            
    else :
        u_form = userupdateform(instance=request.user)
        p_form = userprofileinfoupdteform(instance=profileinfo)

    
    context = {'all_users': all_user,
               'profileinfo': profileinfo,
               'u_form':u_form,
               'p_form': p_form}

    return render(request, 'gis/map.html', context)


def mapwithnewcontent(request):
    usermain = request.user
    id = usermain.id
    profileinfo = userprofile.objects.get(user_id=id)
    all_user = userprofile.objects.all()
    context = {'all_users': list(all_user)}
    json_data = []
    json_data.append(context)
    return JsonResponse(json_data, safe=False)


def layerlistinformation(request):

    cursor = connection.cursor()
    cursor.execute("select * from app_layerlistdata ORDER BY id")
    rows = cursor.fetchall()

    json_data = []
    for row in rows:
        json_data.append({"id": row[0], 'layername': row[1], 'wms': row[2],
                          'wfs': row[3], 'setname': row[4], 'alias_name': row[5], 'datecol': row[6], 'operatorcol': row[7], 'techcol': row[8],'lac': row[9],'ci':row[10],'angle':row[11],'node':row[12],'signal':row[13],'cellname':row[14],'binfields':row[15]})
    return JsonResponse(json_data, safe=False)

def rightclickinfo(request):

    cursor = connection.cursor()
    cursor.execute("select * from right_click")
    rows = cursor.fetchall()

    json_data = []
    for row in rows:
        json_data.append({"type": row[0], 'name': row[1], 'included': row[2],
                          'colorcovminval': row[3], 'colorcovmaxval': row[4], 'colorcovaccepted': row[5], 'colorcovok': row[6], 'colorcovreject': row[7], 'lowcovthresh': row[8],'lowcovcolor': row[9],'overshootingdist':row[10],'overshootingcolor':row[11],'maxservinglinedist':row[12],'autoserving':row[13]})
    return JsonResponse(json_data, safe=False)

def layerlistinformationmysql(request):

    cursor = connections["mysql"].cursor()
    cursor.execute("select count(*) from active_test_download")
    rows = cursor.fetchall()
    context = {'count': rows}
    json_data = []
    json_data.append(context)
    return JsonResponse(json_data, safe=False)


def getstore(request):

    setParam = request.GET.get('sqlParam', None)
    cursor = connection.cursor()
    cursor.execute(setParam)
    rows = cursor.fetchall()

    json_data = []
    for row in rows:
        json_data.append({'data_store':row[0]})
    return JsonResponse(json_data, safe=False)



def profile(request):

    context = {
        'user':  request.user
    }
    return render(request, 'nongis/profile.html', context)


# def update_profile(request):
#     if request.method == 'POST':
#         form = editprofileform(request.POST, instance=request.user)

#         if form.is_valid():
#             form.save()
#             return redirect('map')

#     else:
#         form = editprofileform(instance=request.user)
#         context = {'form': form}
#         return render(request, 'nongis/edit_profile.html', context)


def update_profile(request, user_id):
    user = userbase.objects.get(username=user_id)
    profile = userprofile.objects.get(user=user)
    profileformset = inlineformset_factory(
        userbase, userprofile, fields=('Time_from', 'Time_till', 'operator', 'layers_privillage'))

    if request.method == 'POST':
        form = profileformset(request.POST, instance=user)
        if form.is_valid():
            form.save()
            return redirect('login')

    username = user.username
    form = profileformset(instance=user)
    country = profile.country
    return render(request, 'nongis/edit_profile.html', {'form': form, 'username': username, 'country': country})


def allcountries(self):

    cursor = connection.cursor()
    cursor.execute("SELECT name,mcc FROM public.countries")
    rows = cursor.fetchall()

    json_data = []
    for row in rows:
        json_data.append({"name": row[0], 'mcc': row[1]})
    return JsonResponse(json_data, safe=False)


def alloperators(self):

    cursor = connection.cursor()
    cursor.execute("SELECT name,operatorcode FROM public.operators")
    rows = cursor.fetchall()

    json_data = []
    for row in rows:
        json_data.append({"name": row[0], 'operatorcode': row[1]})
    return JsonResponse(json_data, safe=False)


def forqueryforallattrqueries(request):
    setParam = request.GET.get('sqlParam', None)
    cursor = connection.cursor()
    cursor.execute(setParam)
    rows = cursor.fetchall()
    json_data = []
    for row in rows:
        json_data.append({"query": row[1], "visibility": row[2],"layer": row[3],"ext":row[4], "name": row[5]})
    return JsonResponse(json_data, safe=False)

def forqueryforallaliasnames(request):
    setParam = request.GET.get('sqlParam', None)
    cursor = connection.cursor()
    cursor.execute(setParam)
    rows = cursor.fetchall()
    json_data = []
    for row in rows:
        json_data.append({"alias_name": row[5]})
    return JsonResponse(json_data, safe=False)
    
def forqueryforallattrqueriessp(request):
    setParam = request.GET.get('sqlParam', None)
    cursor = connection.cursor()
    cursor.execute(setParam)
    rows = cursor.fetchall()
    json_data = []
    for row in rows:
        json_data.append({"visibility": row[7], "name": row[5], "sellayer": row[0], "mullayer": row[1], "radiotype": row[2],"qrycriteria": row[3], "geomjson": row[8]})
    return JsonResponse(json_data, safe=False)


def updateinfo(request):
    setParam = request.GET.get('sqlParam', None)
    cursor = connection.cursor()
    cursor.execute(setParam)
    rows = cursor.fetchall()
    json_data = []
    for row in rows:
        json_data.append({"id": row})
    return JsonResponse(json_data, safe=False)


def forsavingqueryindb(request):
    setParam = request.GET.get('sqlParam', None)
    if setParam:
        cursor = connection.cursor()
        cursor.execute(setParam)
        return HttpResponse('updated')
    else:
        return HttpResponse('no setParam')

def forsavinglayerindb(request):
    setParam = request.GET.get('sqlParam', None)
    if setParam:
        cursor = connection.cursor()
        cursor.execute(setParam)
        return HttpResponse('updated')
    else:
        return HttpResponse('no setParam')

def forbulkuploaduserbasic(request):
    setParam = request.GET.get('sqlParam', None)
    if setParam:
        cursor = connection.cursor()
        cursor.execute(setParam)
        return HttpResponse('updated')
    else:
        return HttpResponse('no setParam')


def updateid(request):
    setParam = request.GET.get('sqlParam1', None)
    if setParam:
        cursor = connection.cursor()
        cursor.execute(setParam)
        return HttpResponse('updated')
    else:
        return HttpResponse('no setParam')


def layerlists(self):

    cursor = connection.cursor()
    cursor.execute("select alias_name,id from public.app_layerlistdata ")
    rows = cursor.fetchall()

    json_data = []
    for row in rows:
        json_data.append({"layername": row[0], "id": row[1]})
    return JsonResponse(json_data, safe=False)


def useractiveness(request):
    setParam = request.GET.get('sqlParam', None)

    if setParam:
        cursor = connection.cursor()
        cursor.execute(setParam)
        return HttpResponse('updated')
    else:
        return HttpResponse('no setParam')


def specificprofilemodal(request):
    setParam = request.GET.get('sqlParam', None)

    if setParam:
        cursor = connection.cursor()
        cursor.execute(setParam)
        return HttpResponse('updated')
    else:
        return HttpResponse('no setParam')


def _getUserActiveness(self):

    cursor = connection.cursor()
    cursor.execute("select username,is_active,email,id from public.auth_user ")
    rows = cursor.fetchall()

    json_data = []
    for row in rows:
        json_data.append(
            {"username": row[0], "status": row[1], "email": row[2], 'id': row[3]})
    return JsonResponse(json_data, safe=False)


def _getAllUserProfiles(self):

    cursor = connection.cursor()
    cursor.execute("select * from public.app_userprofileinfo ")
    rows = cursor.fetchall()

    json_data = []
    for row in rows:
        json_data.append(
            {"user_id": row[4], "country": row[1], "operator": row[2], 'layers': row[7], 'time_from': row[5], 'time_till': row[6]})
    return JsonResponse(json_data, safe=False)


def idofbulkcreateduser(request):

    setParam = request.GET.get('sqlParam', None)
    cursor = connection.cursor()
    cursor.execute(setParam)
    rows = cursor.fetchall()
    json_data = []
    for row in rows:
        json_data.append({"id": row[0]})
    return JsonResponse(json_data, safe=False)


def uploaduserprofileinfoforbulkupload(request):
    setParam = request.GET.get('sqlParam', None)
    if setParam:
        cursor = connection.cursor()
        cursor.execute(setParam)
        return HttpResponse('updated')
    else:
        return HttpResponse('no setParam')


def _getAllUserbase(self):

    cursor = connection.cursor()
    cursor.execute("select * from public.auth_user ")
    rows = cursor.fetchall()

    json_data = []
    for row in rows:
        json_data.append(
            {"id": row[0], "username": row[4], "is_active": row[9], 'is_admin': row[3], 'email': row[7]})
    return JsonResponse(json_data, safe=False)


def changebulkuser(request):
    setParam = request.GET.get('sqlParam', None)

    if setParam:
        cursor = connection.cursor()
        cursor.execute(setParam)
        return HttpResponse('updated')
    else:
        return HttpResponse('no setParam')


def queryforcolumnnames(request):
    setParam = request.GET.get('sqlParam', None)
    data_store = request.GET.get('data_store', None)
    if (data_store == "mysql"):
        cursor = connections['mysql'].cursor()
        cursor.execute(setParam)
    else:
        cursor = connection.cursor()
        cursor.execute(setParam)
    rows = cursor.fetchall()
    json_data = []
    for row in rows:
        json_data.append({"coulmns": row[3]})
    return JsonResponse(json_data, safe=False)

def allQuery(request):
    setParam = request.GET.get('sqlParam', None)
    data_store = request.GET.get('data_store', None)
    if (data_store == "mysql"):
        cursor = connections['mysql'].cursor()
        cursor.execute(setParam)
    else:
        cursor = connection.cursor()
        cursor.execute(setParam)
    rows = cursor.fetchall()
    return JsonResponse(rows, safe=False)

def SpatilQuery(request):
    setParam = request.GET.getlist('sqlParam[]',None)
    res = []
    cursor = connection.cursor()
    
    for x in setParam:
        cursor.execute(x)
        rows = cursor.fetchall()
        breakpoint()
        res.append(rows)

    return JsonResponse(rows, safe=False)


def queryforvalues(request):
    setParam = request.GET.get('sqlParam', None)
    data_store = request.GET.get('data_store', None)
    if (data_store == "mysql"):
        cursor = connections['mysql'].cursor()
        cursor.execute(setParam)
    else:
        cursor = connection.cursor()
        cursor.execute(setParam)
    rows = cursor.fetchall()
    json_data = []
    for row in rows:
        json_data.append({"values": row[0]})
    return JsonResponse(json_data, safe=False)

def searchLayer(request):
    table = request.GET.get('table', None)
    field = request.GET.get('field', None)
    sql = "Select "+field+" from "+table+";"
    cursor = connection.cursor()
    cursor.execute(sql)
    rows = cursor.fetchall()
    json_data = []
    for row in rows:
        json_data.append({"values": row[0]})
    return JsonResponse(json_data,safe=False)


# for right click functionality to get all cells with specific lac
def getallcells(request):
    setParam = request.GET.get('sqlParam', None)
    
    cursor = connection.cursor()
    cursor.execute(setParam)
    rows = cursor.fetchall()
    return JsonResponse(rows, safe=False)


def getallcellsmysql(request):
    setParam = request.GET.get('sqlParam', None)
    
    cursor = connections['mysql'].cursor()
    cursor.execute(setParam)
    # transaction.commit_unless_managed(using='mysql')

    rows = cursor.fetchall()
    return JsonResponse(rows, safe=False)


# get all CI value from lac

def getallci(request):
    setParam = request.GET.get('sqlParam', None)
    
    cursor = connection.cursor()
    cursor.execute(setParam)
    rows = cursor.fetchall()
    return JsonResponse(rows, safe=False)


# Timebinning

def searchLayer(request):
    table = request.GET.get('table', None)
    field = request.GET.get('field', None)
    sql = "Select "+field+" from "+table+";"
    cursor = connection.cursor()
    cursor.execute(sql)
    rows = cursor.fetchall()
    json_data = []
    for row in rows:
        json_data.append({"values": row[0]})
    return JsonResponse(json_data,safe=False)

def getTimeBinFields(request):
    """
    table = request.GET.get('table', None)
    #field = request.GET.get('field', None)
    sql = "select COLUMN_NAME from INFORMATION_SCHEMA.COLUMNS where TABLE_NAME = '"+table+"' and (columns.data_type='integer' or columns.data_type='double precision')"
    print(sql)
    #sql = "Select "+field+" from "+table+";"
    cursor = connection.cursor()
    cursor.execute(sql)
    rows = cursor.fetchall()
    json_data = []
    for row in rows:
        print(row[0])
        json_data.append({"values": row[0]})
    return JsonResponse(json_data,safe=False)
    """
    cursor = connection.cursor()
    cursor.execute(
        "Select layername,alias_name,binning_fields from app_layerlistdata;")
    vals = cursor.fetchall()
    bin_data = []
    for val in vals:
        if (val[2]):
            columns = []
            for col in val[2].split(","):
                columns.append(col)
            bin_data.append({"layerName": val[0], "alias": val[1], "columns": columns})
    print(vals)
    return JsonResponse(bin_data, safe=False)

def getTimeBinData(request):
    table = request.GET.get('table', None)
    field = request.GET.get('field', None)
    interval = request.GET.get('interval', None)
    interval = int(interval)
    # interval = "50000"
    operation = request.GET.get('operation', None)
    cursor = connection.cursor()
    print(table,field,interval,operation)
    binData = []
    """
    sql = "Select min(measdate) from "+table+";"
    cursor.execute(sql)
    mindate = cursor.fetchall()[0][0]
    sql = "Select max(measdate) from " + table + ";"
    cursor.execute(sql)
    maxdate = cursor.fetchall()[0][0]
    print(type(mindate),type(maxdate))
    delta = datetime.timedelta(0,int(interval),0)
    tot_bins = int(math.floor((maxdate-mindate).total_seconds()/int(interval)))
    for i in range(tot_bins-1):
        sql = "select ST_X(ST_TRANSFORM((st_centroid(st_union(the_geom))),3857)) AS LONG, ST_Y(ST_TRANSFORM((st_centroid(st_union(the_geom))),3857)) AS LAT, "+operation+"(abs("+field+"))"+" from (select geom as the_geom,"+field+" from "+table+" where measdate>='"+str(mindate+datetime.timedelta(0,i*int(interval),0))+"'::timestamp and measdate<'"+str(mindate+datetime.timedelta(0,(i+1)*int(interval),0))+"'::timestamp)t1"
        # print(sql)
        cursor.execute(sql)
        bin = cursor.fetchall()
        if bin[0][0]:
            # print(bin)
            binData.append([bin[0][0],bin[0][1],float(str(bin[0][2]))])
            print([bin[0][0],bin[0][1],float(str(bin[0][2]))])
    """
    sql = "select measdate,ST_X(ST_TRANSFORM(geom,3857)) AS LONG, ST_Y(ST_TRANSFORM(geom,3857)) AS LAT, "+field+" from "+table+";"
    cursor.execute(sql)
    tbl_data = cursor.fetchall()
    np_arr = np.array(tbl_data)
    print(np_arr[0])
    minDate = np.min(np_arr[:,0])
    maxDate = np.max(np_arr[:,0])
    noOfBins = int((maxDate-minDate).total_seconds()/interval)
    print("noOfBins",noOfBins)
    prevDate = minDate
    if(operation=="sum"):
        for x in range(noOfBins-1):
            nextDate = prevDate+datetime.timedelta(0, interval)
            np_arr_1 = np_arr[(np_arr[:, 0] < nextDate) & (np_arr[:, 0] >= prevDate)]
        # low = datetime.datetime(2019, 4, 16, 18, 42)
        # high = datetime.datetime(2019, 4, 16, 18, 52)
        #     if (len(np_arr[:, 0])>0 and len(np_arr[:, 1])>0 and len(np_arr[:, 2])>0 and len(np_arr[:,3])>0):
            if (len(np_arr_1[:, 0]) > 0 and len(np_arr_1[:, 1]) > 0 and len(np_arr_1[:, 2]) > 0 and len(np_arr_1[:, 3]) > 0):
                centroidLon = np.mean(np_arr_1[:,1])
                centroidLat = np.mean(np_arr_1[:, 2])
                fieldVal = np.sum(np_arr_1[:,3])
                print(centroidLon,centroidLat,fieldVal)
                binData.append([centroidLon, centroidLat, float(fieldVal)])
            prevDate = nextDate
    else:
        for x in range(noOfBins - 1):
            nextDate = prevDate + datetime.timedelta(0, interval)
            np_arr_1 = np_arr[(np_arr[:, 0] < nextDate) & (np_arr[:, 0] >= prevDate)]
            # if(len(np_arr_1[:, 0])>0 and len(np_arr_1[:, 1])>0 and len(np_arr_1[:, 2])>0 and len(np_arr_1[:,3])>0):
            if (len(np_arr_1[:, 0]) > 0 and len(np_arr_1[:, 1]) > 0 and len(np_arr_1[:, 2]) > 0 and len(np_arr_1[:, 3]) > 0):
                try:
                    centroidLon = np.mean(np_arr_1[:, 1])
                    centroidLat = np.mean(np_arr_1[:, 2])
                    fieldVal = np.mean(np.abs(np_arr_1[:, 3]))
                    print(centroidLon, centroidLat, fieldVal)
                    binData.append([centroidLon,centroidLat,float(fieldVal)])
                except Exception as e:
                    print("prevDate",prevDate,"nextDate",nextDate)
                    print(np_arr_1[:,1])
            prevDate = nextDate

    return JsonResponse(binData, safe=False)
    #Time binning end


# Identify get layers and fields
def getIdentifyFields(request):
    cursor = connection.cursor()
    cursor.execute("Select layername,alias_name,identify_fields,identify_fields_alias from app_layerlistdata")
    # cursor.execute("Select layername,alias_name,identify_fields,identify_fields_alias from app_layerlistdata where layername != 'rfsig_tests_hexabins'")
    vals = cursor.fetchall()
    id_data = []
    print("Identify Fields")
    for val in vals:
        if(val[2]):
            columns = []
            columns_alias = []
            for col in val[2].split(","):
                columns.append(col)
            print(val)
            if(val[3]):
                for col in val[3].split(","):
                    columns_alias.append(col)
            else:
                for col in val[2].split(","):
                    columns_alias.append(col)
            id_data.append({"layerName":val[0],"alias":val[1],"columns":columns,"columns_alias":columns_alias})
    # print(vals)
    return JsonResponse(id_data,safe=False)

# Search get layers and fields
def getSearchFields(request):
    cursor = connection.cursor()
    cursor.execute("Select layername,alias_name,search_fields from app_layerlistdata")
    vals = cursor.fetchall()
    search_data = []
    for val in vals:
        if(val[2]):
            search_data.append({"layerName":val[0],"alias":val[1],"field":val[2]})
    # print(vals)
    # print(search_data)
    return JsonResponse(search_data,safe=False)

def getBinningLayers(request):
    cursor = connection.cursor()
    cursor.execute("Select layername, geobinning_exists from app_layerlistdata;")
    vals = cursor.fetchall()
    binning_layers = []
    for val in vals:
        binning_layers.append({"layerName":val[0],"exists":val[1]})
    print(binning_layers)
    return JsonResponse(binning_layers,safe=False)

# Get geobinning data
def points_to_bins(df, resolution,latitude_col_name,longitude_col_name,feature_col_name,testtype_col_name):
    
    '''Use h3.geo_to_h3 to index each data point into the spatial index of the specified resolution.
      Use h3.h3_to_geo_boundary to obtain the geometries of these hexagons'''

    df = df[[latitude_col_name,longitude_col_name,feature_col_name,testtype_col_name]].copy()

    df["hex_id"] = df.apply(lambda row: h3.geo_to_h3(row[latitude_col_name], row[longitude_col_name], resolution), axis = 1)

    df_aggreg = df.groupby(by = ["hex_id",testtype_col_name]).agg({feature_col_name: ['mean', 'count']}).reset_index()
    df_aggreg.columns = ["hex_id",testtype_col_name, "value","count"]
    df_aggreg['bin_size'] = resolution

    geom =  df_aggreg.hex_id.apply(lambda x: Polygon(h3.h3_to_geo_boundary(h3_address=x,geo_json=True)) )
    crs = {'init': 'epsg:4326'}
    gpdf_aggreg = gp.GeoDataFrame(df_aggreg,geometry=geom,crs=crs)
    return gpdf_aggreg

def points_to_bins_no_val(df, resolution,latitude_col_name,longitude_col_name):
    
    '''Use h3.geo_to_h3 to index each data point into the spatial index of the specified resolution.
      Use h3.h3_to_geo_boundary to obtain the geometries of these hexagons'''

    df = df[[latitude_col_name,longitude_col_name]].copy()

    df["hex_id"] = df.apply(lambda row: h3.geo_to_h3(row[latitude_col_name], row[longitude_col_name], resolution), axis = 1)

    df_aggreg = df.groupby(by = ["hex_id"]).agg({latitude_col_name: ['count']}).reset_index()
    df_aggreg.columns = ["hex_id", "count"]
    df_aggreg['bin_size'] = resolution

    geom =  df_aggreg.hex_id.apply(lambda x: Polygon(h3.h3_to_geo_boundary(h3_address=x,geo_json=True)) )
    crs = {'init': 'epsg:4326'}
    gpdf_aggreg = gp.GeoDataFrame(df_aggreg,geometry=geom,crs=crs)
    return gpdf_aggreg

@csrf_exempt
def getGeoBins(request):
    # table_to_fetch = request.GET.get('layer','')
    # technology = request.GET.getlist('technology[]','')
    # fromDate = request.GET.get('fromDate','')
    # toDate = request.GET.get('toDate','')
    # operator = request.GET.getlist('operator[]','')
    table_to_fetch = request.POST.get('layer','')
    technology = request.POST.getlist('technology[]','')
    fromDate = request.POST.get('fromDate','')
    toDate = request.POST.get('toDate','')
    operator = request.POST.getlist('operator[]','')
    print('operators',operator)
    currentDir = os.path.dirname(os.path.realpath(__file__))
    configFile=open(currentDir+"/geobinning_config.json")
    parameters = configFile.read()
    parameters = json.loads(parameters)
    print(parameters[table_to_fetch]["value_col"])
    connection = parameters[table_to_fetch]["connection"]
    # table_to_fetch = parameters["table_to_fetch"]
    # table_to_push = parameters["table_to_push"]
    latitude_col = parameters[table_to_fetch]["latitude_col"]
    longitude_col = parameters[table_to_fetch]["longitude_col"]
    date_col = parameters[table_to_fetch]["date_col"]
    operator_col = parameters[table_to_fetch]["operator_col"]
    technology_col = parameters[table_to_fetch]["technology_col"]
    value_col = parameters[table_to_fetch]["value_col"]
    test_type_col = parameters[table_to_fetch]["test_type_col"]
    engine = create_engine(connection)
    if(value_col):
        sql = "select "+latitude_col+","+longitude_col+","+value_col+","+test_type_col+" from "+table_to_fetch
    else:
        sql = "select "+latitude_col+","+longitude_col+" from "+table_to_fetch
    where_clause = False    # whether where clause already added
    if(fromDate and toDate):
        sql += " where "+date_col+"<'"+toDate+"' and "+date_col+">'"+fromDate+"'"
        where_clause = True
    if(technology):
        if(where_clause):
            if(len(technology)>1):
                sql += " and ("+technology_col+"='"+technology[0]+"'"
                for i in range(1,len(technology)):
                    sql += " or "+technology_col+"='"+technology[i]+"'"
                sql += ")"
            else:
                sql += " and "+technology_col+"='"+technology[0]+"'"
        else:
            sql += " where "
            where_clause = True
            if(len(technology)>1):
                sql += "("+technology_col+"='"+technology[0]+"' or "
                for i in range(1,len(technology)):
                    sql += technology_col+"='"+technology[i]+"' or "
                sql = sql[:-4]
                sql+=")"
            else:
                sql += technology_col+"='"+technology[0]+"'"
    if(operator):
        if(where_clause):
            if(len(operator)>1):
                sql += " and ("+operator_col+"='"+operator[0]+"'"
                for i in range(1,len(operator)):
                    sql += " or "+operator_col+"='"+operator[i]+"'"
                sql += ")"
            else:
                sql += " and "+operator_col+"='"+operator[0]+"'"
        else:
            sql += " where "
            where_clause = True
            if(len(operator)>1):
                sql += "("+operator_col+"='"+operator[0]+"' or "
                for i in range(1,len(operator)):
                    sql += operator_col+"='"+operator[i]+"' or "
                sql = sql[:-4]
                sql+=")"
            else:
                sql += operator_col+"='"+operator[0]+"'"
    sql+=";"
    print(sql)
    data = pd.read_sql_query(sql,engine)
    if(len(data)>0):
        # gphex_3 = points_to_bins(df = data , resolution = 3,latitude_col_name=latitude_col,longitude_col_name=longitude_col, feature_col_name=value_col,testtype_col_name   = test_type_col)
        # gphex_3 = gphex_3.to_json()
        # gphex_4 = points_to_bins(df = data , resolution = 4,latitude_col_name=latitude_col,longitude_col_name=longitude_col, feature_col_name=value_col,testtype_col_name   = test_type_col)
        # gphex_4 = gphex_4.to_json()
        # gphex_5 = points_to_bins(df = data , resolution = 5,latitude_col_name=latitude_col,longitude_col_name=longitude_col, feature_col_name=value_col,testtype_col_name   = test_type_col)
        # gphex_5 = gphex_5.to_json()
        # gphex_6 = points_to_bins(df = data , resolution = 6,latitude_col_name=latitude_col,longitude_col_name=longitude_col, feature_col_name=value_col,testtype_col_name   = test_type_col)
        # gphex_6 = gphex_6.to_json()
        arr = []
        for i in range(3,7):
            if(value_col):
                temp_arr = points_to_bins(df = data , resolution = i,latitude_col_name=latitude_col,longitude_col_name=longitude_col, feature_col_name=value_col,testtype_col_name = test_type_col)
            else:
                temp_arr = points_to_bins_no_val(df = data , resolution = i,latitude_col_name=latitude_col,longitude_col_name=longitude_col)
            temp_arr = temp_arr.to_json()
            arr.append(temp_arr)
        try:
            print("*******response acquired******")
            return JsonResponse(arr,safe=False)
        except Exception as e:
            engine.dispose()
            print(e)
            return JsonResponse("error",safe=False)
    else:
        return JsonResponse({},safe=False)

def getLayerOperators(request):
    cursor = connection.cursor()
    layer = request.GET.get('layer','')
    if(layer):
        cursor.execute("Select distinct(measoperatorcode) from "+layer+";")
        operators = cursor.fetchall()
        json_data = []
        for row in operators:
            json_data.append({"values": row[0]})
        return JsonResponse(json_data,safe=False)

    


def layerconfigview(request):
    with open('static/custom/gis/layerlist/layerconfig.json') as json_file:
        json_data = json_file.read()
        obj = json.loads(json_data)
    return JsonResponse(obj)