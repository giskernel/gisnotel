from django.db import models
from django.conf import settings
from django.contrib.auth.models import User
from django.db.models.signals import post_save
# Create your models here.
from datetime import datetime, timedelta


def get_oldDate():
    return datetime.today() - timedelta(days=7)


def get_currentDate():
    return datetime.today()


class UserProfileInfo(models.Model):
    user = models.OneToOneField(User, on_delete=models.CASCADE)

    # additional things

    country = models.CharField(max_length=500, blank=True)
    operator = models.CharField(max_length=500, blank=True)
    Time_from = models.DateField(default=get_oldDate)
    Time_till = models.DateField(default=get_currentDate)
    layers_privillage = models.CharField(max_length=500, blank=True)
    profile_pic = models.ImageField(
        default='default.jpg', upload_to='profile_pics', blank=True)

    def __str__(self):
        return self.user.username


class LayerListData(models.Model):

    layername = models.CharField(max_length=500, blank=True)
    wms = models.CharField(max_length=2000, blank=True)
    wfs = models.CharField(max_length=2000, blank=True)
    setname = models.CharField(max_length=500, blank=True)
