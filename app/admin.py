from django.contrib.auth.models import Group
from app.models import UserProfileInfo
from django.contrib import admin
from app.models import UserProfileInfo, LayerListData
# Register your models here.


admin.site.register(LayerListData)
# Register your models here.

admin.site.site_header = 'N-Hub Optimizer'


class UserProfadmin(admin.ModelAdmin):
    list_display = ('user', 'country', 'operator', 'Time_from',
                    'Time_till', 'layers_privillage')
    list_filter = ('country', 'operator')


admin.site.register(UserProfileInfo, UserProfadmin)
admin.site.unregister(Group)
