# Generated by Django 2.2.1 on 2019-07-09 06:17

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('app', '0017_auto_20190709_1138'),
    ]

    operations = [
        migrations.AlterField(
            model_name='userprofileinfo',
            name='Time_from',
            field=models.DateTimeField(null=True),
        ),
        migrations.AlterField(
            model_name='userprofileinfo',
            name='Time_till',
            field=models.DateTimeField(null=True),
        ),
    ]
