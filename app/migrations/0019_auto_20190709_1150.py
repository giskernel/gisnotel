# Generated by Django 2.2.1 on 2019-07-09 06:20

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('app', '0018_auto_20190709_1147'),
    ]

    operations = [
        migrations.AlterField(
            model_name='userprofileinfo',
            name='Time_from',
            field=models.DateField(blank=True),
        ),
        migrations.AlterField(
            model_name='userprofileinfo',
            name='Time_till',
            field=models.DateField(blank=True),
        ),
    ]
