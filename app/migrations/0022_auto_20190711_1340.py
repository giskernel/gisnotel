# Generated by Django 2.2.1 on 2019-07-11 08:10

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('app', '0021_merge_20190711_1110'),
    ]

    operations = [
        migrations.AlterField(
            model_name='userprofileinfo',
            name='Time_from',
            field=models.DateField(blank=True),
        ),
        migrations.AlterField(
            model_name='userprofileinfo',
            name='Time_till',
            field=models.DateField(blank=True),
        ),
    ]
