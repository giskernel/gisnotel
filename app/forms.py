from django import forms
from django.contrib.auth.models import User
from app.models import UserProfileInfo
from captcha.fields import CaptchaField
from django.contrib.auth.forms import UserChangeForm


class DateTimeInput(forms.DateTimeInput):
    input_type = 'datetime'


class UserForm(forms.ModelForm):
    password = forms.CharField(
        widget=forms.PasswordInput(attrs={'placeholder': 'Password'}))

    #is_active = forms.BooleanField(default=False)

    class Meta():
        model = User
        fields = ("username", 'email', 'password')

    def clean_email(self):
        email = self.cleaned_data.get('email')
        username = self.cleaned_data.get('username')
        if email and User.objects.filter(email=email).exclude(username=username).exists():
            raise forms.ValidationError(u'Email addresses must be unique.')
        return email

    def clean_password(self):
        password = self.cleaned_data.get("password")
        if len(password) < 8:
            raise forms.ValidationError(
                u'Password must be 8 charachters or more')
        if password.isdigit():
            raise forms.ValidationError(
                u'Password must be combination of Alphabets and numbers')
        return password


class UserProfileInfoForm(forms.ModelForm):
    captcha = CaptchaField()

    class Meta():
        model = UserProfileInfo
        fields = ('profile_pic',)


class userupdateform(forms.ModelForm):
    email = forms.EmailField()

    class Meta():
        model = User
        fields = ("username", 'email')


class userprofileinfoupdteform(forms.ModelForm):

    class Meta():
        model = UserProfileInfo
        fields = ('profile_pic',)
